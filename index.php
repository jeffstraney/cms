<?php
namespace Jasper;

// to use a session, you must call session start at the beginning of a script.
session_start();

// include the apps settings
include "settings.inc";

// include helper functions 
include "includes/helpers.inc";

// set token for anon users in session
set_csrf_token_anon();

// user object 
global $user;

$user_controller = new Controller\UserController();

$user = $user_controller->get_current_user();

$site_root = $configs['site_root'];

$site_title = $configs['site_title'];

// subtitle is decided by entity class if any exists
$sub_title = "";

// any additional css classes added to top level element, depending on which 
// controller is performing the current action (e.g. class="user create")
$controller_action_css_class = "";

//fallback, echo empty string if no controller method found.
$template = "";

// create a new router
$router = new Router();

// get query parameters. passed by .htaccess
$route = $_GET['q'];

// the main view will be included here based on the routers interpretation
// of the url. I've actually 'cut' off the part of the url we need and put
// it in the script as GET parameters using the .htaccess file
$router->decode_requested_route($route);

// when matches_route is called, the first match in the router determines what
// controller we want to use, what method (action) to call on the controller
// and splits the url into arguments for that method
if ($router->matches_route()) {

  // view, view_all, get_create_form, get_update_form ...
  $action = $router->get_action();

  // UserController, EventController, UniversityController
  $controller_class = 'Jasper\\Controller\\' . $router->get_controller_class();

  // '/user/12' = array('user', '12'), '/universities' = array('universities'). Note there is some redundancy here
  // but I think it makes sense to use non-associative arrays here (order is implied)
  $arguments = $router->get_arguments();

  // instantiate the controller class using a variable name
  $controller = new $controller_class();

  // echo the result of that classes method
  $template = $controller->{$action}($arguments);

  // check if the controller implements get_action_title
  $has_action_title = get_val($controller, "get_action_title", NULL);

  if ($has_action_title !== NULL) {

    // get subtitle from controller based on action
    $subtitle = $controller->get_action_title($action, $arguments);

    $site_title .= $subtitle;

  }

  $has_action_css_class = get_val($controller, 'get_action_css_class', NULL);

  if ($has_action_css_class !== NULL) {

    $controller_action_css_class = $controller->get_action_css_class($action, $arguments);
    
  }

}

?>
<!DOCTYPE html>
<html>

  <?php include "includes/partials/head.inc"; ?>

  <body>

  <?php include "includes/partials/header.inc"; ?>

  <div id="main-view" class="<?php echo $controller_action_css_class; ?>">

    <?php 

    // echo the site message if it exists.
    $message = get_message_html();

    echo $message;

    // echo template provided by chosen controller
    echo $template;

    ?>

  </div>

  <?php include "includes/partials/footer.inc"; ?>

  </body>

</html>
