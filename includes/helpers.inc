<?php
// place to put various helper functions

// logs the variable out to the webpage in a pretty printed fashion
function console_log($var) {
  print "<pre>";
  var_dump($var);
  print "</pre>";
}
                                                                      
// encrypts a plain-text password into a hash before inserting into database
function encrypt_password($pw) {

  $hash = password_hash($pw, PASSWORD_BCRYPT , array('cost' => 12));

  return $hash;

}

function set_csrf_token_anon () {

  // check last activity. If last activity is not set, use the current time
  $last_activity = get_val($_SESSION, 'last_activity', time());

  // set the last activity time
  $_SESSION['last_activity'] = $last_activity;

  // refresh the anon token every 30 minutes of inactivity. also logs the 
  // user out as a side effect essentially.
  if ( time() - $last_activity > 1800 ) {

    session_unset();     

    session_destroy();

  }

  // set csrf token if first time visiting, or if session expired
  $new_token = bin2hex(random_bytes(32));

  $_SESSION['csrf_token_anon'] = get_val($_SESSION, 'csrf_token_anon', $new_token); 

}

// gets token for anonymous users
function get_csrf_token_anon () {

  return get_val($_SESSION, 'csrf_token_anon', FALSE);

}

// gets token for authenticated users
function get_csrf_token_auth () {

  return get_val($_SESSION, 'csrf_token_auth', FALSE);

}

// checks a hashed password against a plain-text one.
function check_password($pw, $hash) {

  return password_verify($pw, $hash);

}

// provide iterable, a key, and an optional default
// replaces the idiomatic "isset(x[y])? x[y]: null;"
function get_val_ifset($params, $key, $default = NULL) {

  // the function checks for 'emptiness' (i.e. fails on 0 or "", or array())
  // if you want to default on an empty value, simply pass it as $default in the parameters.
  if (is_object($params)) {

    if (property_exists($params, $key)) {

      return $params->$key;

    }
    else if (method_exists($params, $key)) {

      return TRUE;

    }

    return $default;

  }
  if (is_array($params)) {

    return isset($params[$key])? $params[$key]: $default;

  }

}

// this does exactly what get_val_ifset does but will return $default on empty values
// (e.g. "", 0, FALSE)
function get_val($params, $key, $default) {
  // the function checks for 'emptiness' (i.e. fails on 0 or "", or array())
  // if you want to default on an empty value, simply pass it as $default in the parameters.
  if (is_object($params)) {

    if (property_exists($params, $key) && !empty($params->{$key})) {

      return $params->{$key};

    }
    else if (method_exists($params, $key)) {

      return TRUE;

    }

    return $default;

  }
  if (is_array($params)) {

    // returns default on 'empty' or 'falsey' values.
    return isset($params[$key]) && !empty($params[$key])? $params[$key]: $default;

  }


}

// echos a failure response in json.
function json_response ($key = 'data', $msg = '') {

  $json = array();

  $json[$key] = $msg;

  header("Content-Type: application/json");

  echo json_encode($json);

  die();

}

// this function simply returns the string of the path
// to the entity partial from its partials directory.
function get_entity_partial ($partial) {

  global $configs;

  $entity_partial_dir = $configs['entity_partial_dir'];

  return "$entity_partial_dir/$partial.inc";

}

function get_mail_partial ($partial) {

  return "partials/mail/$partial.inc";

}

function get_entity_js ($js_file) {

  $script = file_get_contents($js_file);

  console_log($script);

  $js = "<script>";
  $js .= $script;
  $js .= "</script>";

  return $js;

}

function send_auth_email ($username, $email, $auth_url) {

  include get_mail_partial("auth");

  mail($email, "Welcome to website", $template);

}

function go_home () {

  global $configs;  

  $site_root = $configs['site_root'];

  header("location: $site_root");

}

// goes back to the refering page. useful for errors arising from
// submitting a form (e.g. submitting a new user. if a PDO exception
// is thrown, this function is called sending the user back to
// the form with an error message)
function go_back () {

  if (!isset($_SERVER['HTTP_REFERER'])) {

    go_home;

  }

  $referer = $_SERVER['HTTP_REFERER'];

  header("location: $referer");
  die();

}

// all entities with a 'view' action will usually follow the
// url pattern of 'resource/[id]', this just performs a redirect
// to the entity type with the following id.
function redirect_entity_view($resource_name, $id) {

  global $configs;

  $site_root = $configs['site_root'];

  // redirect to user/12, university/3, etc.
  header("location: $site_root/$resource_name/$id");
  die();

}

// User message. Appears at the top of the screen after a redirect.
// is set using the session global and is cleared after it is retrieved.
// the 'type' is used for styling purposes.
function set_message ($message, $message_type = "info") {

  // return false if there is no session
  if (!isset($_SESSION)) {
    return FALSE;
  }

  // set the message and type to be used on the next page.
  $_SESSION['message'] = $message;

  $_SESSION['message_type'] = $message_type;


}

// clears message so the message doesn't persist over multiple page requests.
function clear_message () {
  unset($_SESSION['message']);
  unset($_SESSION['message_type']);
}

function get_message_html () {

  if (!isset($_SESSION['message'])) {
    return "";
  }

  $message = $_SESSION["message"];
  $message_type = $_SESSION["message_type"];

  include "partials/message.inc";

  // clear message info from the session;
  clear_message();

  // return template defined in the include file.
  return $template;

}

// redirects to a path relative to the site root.
// you could user "login" for example and the "http://cop4710_project/" part is included.
function redirect($url) {

  global $configs;

  $site_root = $configs['site_root'];

  header("location: $site_root/$url");

  die();

}

function get_pagination_from_id ($id, $inc) {
  // calculates the start and end on an id.
  $start = (int)((int)$id / $inc) * $inc;
  $end = ($start / $inc + 1) * $inc;

  return array($start, $end);

}

function sanitize_email_input (&$email, &$username) {

  $email = sanitize_xss($email);

  $username = sanitize_xss($username);

  $email = str_replace(array('\n', '\r'), '', $email);

  $username = str_replace(array('\n', '\r'), '', $username);

}

// strip out html tags from user input.
const FULL_HTML = 2;
const RESTRICT_HTML = 1;
const NO_HTML = 0;

function sanitize_xss ($input, $sanitization = NO_HTML) {

  global $configs;

  $xss_white_list = get_val($configs, 'xss_white_list', array());

  $xss_white_list = implode($xss_white_list);

  if ($sanitization == FULL_HTML) {

    return $input;

  }
  else if ($sanitization == RESTRICT_HTML) {

    return strip_tags($input, $xss_white_list);  

  }

  return strip_tags($input);  

}

// cleans all parameters in an array of markup. Based on the OWASP
// specifications, this is the safest course of action. White listing
// is somewhat effective, but consider that a sophisticated attacker
// can use an attribute to execute code in an approved tag 
function sanitize_xss_params (&$params = array()) {

  foreach ($params as $key => $parameter) {

    $params[$key] = sanitize_xss($parameter);

  }

}

?>
