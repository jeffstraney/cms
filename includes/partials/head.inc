<head>

  <title><?php echo $site_title; ?></title>

  <!-- meta -->
  <meta name="description" content=""/>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

  <!-- stylesheets -->
  <link rel="stylesheet" type="text/css" href="<?php echo $site_root; ?>/style/css/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $site_root; ?>/js/lib/jquery-ui/jquery-ui.css">
  <script type="application/javascript" src="<?php echo $site_root; ?>/js/lib/jquery/jquery-3.2.0.js"></script>

  <script>
  // define global app
  var app = app || {};
  
  // define scope
  app.scope = {};

    (function () {
      // pass application globals here from php environment
      app.site_root = "<?php echo $site_root; ?>";
      app.scope.csrf_token_anon = "<?php echo get_csrf_token_anon(); ?>";
      app.scope.csrf_token_auth = "<?php echo get_csrf_token_auth(); ?>";
      app.scope.user = <?php echo json_encode($user); ?>;
    })();
  </script>

  <!-- javascript -->
  <script type="application/javascript" src="<?php echo $site_root; ?>/js/dist/app.js"></script>

</head>
