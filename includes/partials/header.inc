<?php
// global configs associative array
global $configs;

// use the site root to construct the links in the header below
// if we moved our site out of our local apache setup, the url would
// change and so we have the base url in a configuration
$site_root = $configs['site_root'];

?>
<div id="header">

  <a href="<?php echo $site_root; ?>" >
    <img class="logo" src="<?php echo $site_root; ?>/res/icons/logo.png"></img>
  </a>
  <ul class="menu">
  <?php
    // check if user is anonymous, or 0
    if (!isset($user)) {
      // echo the login and create account links if the user is anonymous
      echo '<li><a href="'.$site_root.'/login">Login</a></li>';
      echo '<li><a class="waves-effect waves-light btn" href="'.$site_root.'/user/new">New Account</a></li>';
    } else {
      $user_name = get_val($user, 'username', 'Anonymous');
      $uid = get_val($user, 'id', 0);
      // echo links available to the user
      echo '<li>Hi, <a href="'.$site_root.'/user/'.$uid.'">'.$user_name.'</a></li>';
      echo '<li><a href="'.$site_root.'/logout/'.get_csrf_token_auth().'">Logout</a></li>';
    }
  ?>
  </ul>
</div>
