<?php

  global $configs;

  $site_root = $configs['site_root'];

?>
<div id="footer">
  <ul class="menu">
    <h4 >Top</h4>
    <li><a  href="<?php echo $site_root; ?>/">Home</a></li>
    <li><a  href="<?php echo $site_root; ?>/about">About</a></li>
    <?php
    if (isset($user)) {
      echo '<li><a href="'.$site_root.'/logout/'.get_csrf_token_auth().'">Logout</a></li>';
    }
    else {
      echo '<li><a href="'.$site_root.'/login">Login</a></li>';
      echo '<li><a href="'.$site_root.'/user/new">Join</a></li>';
    }
    ?>
  </ul>
  <ul class="menu">
    <h4 >View</h4>
    <li><a href="<?php echo $site_root; ?>/users/">Users</a></li>
  </ul>
  <ul class="menu">
    <h4 >Technical</h4>
    <li><a href="<?php echo $site_root; ?>/help">Help</a></li>
    <li><a href="<?php echo $site_root; ?>/agreement">Agreement</a></li>
    <li><a href="<?php echo $site_root; ?>/privacy">Privacy</a></li>
  </ul>
</div>
