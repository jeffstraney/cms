<?php
namespace Jasper;

// The router class will take an argument passed in the query string (passed as
// parameter 'q' by htaccess for GET requests). Post request use $_POST super global
class Router {

  // When creating your own routes consider that GET requests can be cached and bookmarked.
  // while POST request do not. Additionally, POST requests traditionally have their parameters
  // passed into the body of the document (does not really hide them, but it is often more
  // succinct than using a query string.)

  // lists of routes, implicitly white-listed.
  private $routes = array(
    // routes for GET requests
    'GET' => array(), 
    // routes for POST requests
    'POST' => array(), 
  ); 

  // true if there is a match
  private $found_match;

  // these are our resources. used to auto-generate routes (create, read, update, destroy)
  private $resources = array(
    'permission',
    'role'
  );

  // creates GET paths for view, edit, drop, make in addition to the CRUD paths above
  private $entities = array(
    'user',
    'page',
  );

  // the router maps a route to a method on a controller class.
  private $action;

  // the controller which will call this action method.
  private $controller;

  // simply the GET request split at the /. "user/12" is array("user", "12");
  // or the parameters of the POST request, in addition to the tokenized URL
  // e.g.
  // GET http:://site.com/user/new = array( 0 => 'user', 1 => 'new')
  // POST http:://site.com/user/create = array( 0 => 'user', 1 => 'create', 'param1' => ...)
  private $arguments;

  // helper method to generate resourceful routes.  makes 5 routes for the resource
  // new, view, view_all, delete, edit. The resource name is white listed, so an attacker
  // could not create routes simply by sending a request.
  private function create_resourceful_get_routes ($resource_name) {

    $class = ucfirst($resource_name);

    return array(
      "/^$resource_name\/new$/" => array('make', "{$class}Controller"),
      "/^$resource_name\/([0-9]+)\/edit$/" => array('edit', "{$class}Controller"),
      "/^$resource_name\/([0-9]+)\/delete$/" => array('drop', "{$class}Controller"),
      "/^$resource_name\/([0-9]+)$/" => array('view', "{$class}Controller"),
      "/^{$resource_name}s$/" => array('view_all', "{$class}Controller")
    );

  }

  // creates the resourceful POST routes which are create, update, destroy
  private function create_resourceful_post_routes ($resource_name) {

    $class = ucfirst($resource_name);

    // creates simple CRUD routes. If you want a 'read_all' method or
    // a special route to modify the resource in a particular way, you can
    // simply append a route to the $routes array in the __construct method
    return array(
      "/^$resource_name$/" => array('read', "{$class}Controller"),
      "/^$resource_name\/create$/" => array('create', "{$class}Controller"),
      "/^$resource_name\/update$/" => array('update', "{$class}Controller"),
      "/^$resource_name\/destroy$/" => array('destroy', "{$class}Controller"),
    );

  }


  public function __construct() {

    $this->found_match = FALSE;

    // auto generate resourceful routes from the resource names
    foreach ($this->resources as $resource_name) {

      $this->routes['POST'] += $this->create_resourceful_post_routes($resource_name);

    }
    foreach ($this->entities as $entity_name) {

      $this->routes['POST'] += $this->create_resourceful_post_routes($entity_name);
      $this->routes['GET'] += $this->create_resourceful_get_routes($entity_name);

    }

    // returns password reset form
    $this->routes['GET']['/^user\/password-reset$/'] = array('password_reset_form', 'UserController');
    // performs the action of reseting the password
    $this->routes['POST']['/^user\/password-reset$/'] = array('password_reset', 'UserController');
    $this->routes['GET']['/^login$/'] = array('make', 'SessionController');

    // log out requires a 60 digit token
    $this->routes['GET']['/^logout\/[0-9a-f]{64}$/'] = array('drop', 'SessionController');

    // routes used to create sessions after submitting login/logout
    $this->routes['POST']['/^session\/create$/'] = array('create', 'SessionController');
    $this->routes['POST']['/^session\/destroy$/'] = array('destroy', 'SessionController');

    // view the admin permissions page
    $this->routes['GET']['/^admin\/permissions$/'] = array('administrate', 'PermissionController');
    // view the admin role page
    $this->routes['GET']['/^admin\/roles$/'] = array('administrate', 'RoleController');

    // resources don't have a 'read_all' method by default
    $this->routes['POST']['/^permission\/read_all$/'] = array('read_all', 'PermissionController');
    $this->routes['POST']['/^permission\/roles$/'] = array('read_permission_roles', 'PermissionController');

    // allows ajaxified roles to be read.
    $this->routes['POST']['/^role\/read_all$/'] = array('read_all', 'RoleController');

    $this->routes['POST']['/^role\/permission\/create$/'] = array('role_permission_create', 'RoleController');
    $this->routes['POST']['/^role\/permission\/destroy$/'] = array('role_permission_destroy', 'RoleController');

    $this->routes['POST']['/^user\/read_all$/'] = array('read_all', 'UserController');
    $this->routes['POST']['/^user\/status$/'] = array('change_status', 'UserController');

    // 60 character hex code for authentication. generated using random bytes.
    $this->routes['GET']['/^user\/auth\/[0-9a-f]{60}$/'] = array('auth', 'UserController');
    $this->routes['GET']['/^admin\/users$/'] = array('administrate', 'UserController');
   
    // it is also possible to map multiple routes to a controllers action. Here we can
    // get a page by its page id, or by its string name
    $this->routes['GET']['/^.*$/'] = array('view', 'PageController', 'page');
    
  }

  // break url into tokens. determine if request is GET or POST
  public function decode_requested_route($route) {

    $method = $_SERVER['REQUEST_METHOD'];

    if (!isset($this->routes[$method])) {

      // handle error. request is neither POST or GET 
      return;

    }

    $possible_routes = $this->routes[$method];

    // look for first match.
    foreach($possible_routes as $exp => $args) {

      $is_match = preg_match($exp, $route);

      if ($is_match === 1) {

        $this->found_match = TRUE;

        $this->action = $args[0];

        $this->controller = $args[1];

        $this->arguments = explode('/', $route);

        break;

      }

    }

    return;

  }

  function matches_route () {

    return $this->found_match;

  }

  // gets the action or method of the controller that is mapped to the route
  // for example (view, view_all, get_edit_form). This is usually something
  // fullfilled by a GET request. For POSTing data, the controller will return
  // a form through this action.
  function get_action () {

    return $this->action; 

  }

  function get_controller_class () {

    return $this->controller;

  }


  function get_arguments() {

    if (!isset($this->arguments)) {

      return array();

    }

    return $this->arguments;

  }

}

?>
