<?php
namespace Jasper;

class Database {

  public static $pdo = FALSE;

  public function __construct () {

  }

  private function get_pdo () {

    if (Database::$pdo === FALSE) {

      Database::$pdo = new \Jasper\Database\CustomPDO();

    }

    return Database::$pdo;

  }

  public function query ($query) {

    $pdo = $this->get_pdo();

    return $pdo->query($query);

  }

  public function prepared_query ($query, $params) {

    $pdo = $this->get_pdo();

    try {
      $result = $pdo->prepared_query($query, $params);

      return $result;
    }
    catch (PDOException $e) {

      // uncomment two statement below to debug
       console_log($e);
       console_log($e->getMessage());
       die();

      $msg = $e->getMessage();

      set_message($msg, 'error');
      // go back to last page.
      go_back();

      die();

    }
    catch (Exception $e) {

      // uncomment two statement below to debug
       console_log($e->getMessage());
       die();
      
      $msg = $e->getMessage();

      set_message($msg, 'error');
      // go back to last page.
      go_back();

      die();

    }

  }

  public function beginTransaction () {

    $pdo = $this->get_pdo();

    $pdo->beginTransaction();

  }

  public function commit () {

    $pdo = $this->get_pdo();
  
    $pdo->commit();
    
  }

}

?>
