app.template('administrate_role', function (record) {

  record = record || {};

  var id = record.id;

  var name = record.name;
  
  var label = record.label;

  // template to edit a role
  var template = $('\
    <div class="record role" data-role="'+id+'">\
      <span class="field label">' + label + '</span>\
      <span class="field name">' + name + '</span>\
      <input style="display:none" class="label" type="text" value="' + label + '"/>\
      <input style="display:none" class="name" type="text" value="' + name + '"/>\
      <div class="button edit">Edit</div>\
      <div style="display:none" class="button save">Save</div>\
      <div class="button destroy">Delete</div>\
    </div>\
  ');

  // return the jQuery object
  return template;

});
