app.controller(function(scope) {

  var api_endpoint = scope.api_endpoint;

  // api endpoints for the RoleController
  function read_all_roles (params) {

    return api_endpoint(params, {
      url: "/role/read_all",
    });

  } 

  function read_role (params) {

    return api_endpoint(params, {
      url: "/role/read",
    });

  }

  function create_role (params) {

    return api_endpoint(params, {
      url: "/role/create",
    });

  }

  function update_role (params) {

    return api_endpoint(params, {
      url: "/role/update",
    });

  }

  function destroy_role (params) {

    return api_endpoint(params, {
      url: "/role/destroy",
    });

  }

  // aggregate of roles in the administrate roles view
  var role_aggregate = $('div.administrate-role.aggregate.roles');

  // form to create a new role
  var create_form = $('form#create-role');

  app.machine_name_text_field($('input.name', create_form), {
    source: $('input.label', create_form)
  });

  create_form.submit(function (e) {

    e.preventDefault();

    var self = $(this);

    var name_val = $('input.name', self).val();
    var label_val = $('input.label', self).val();

    create_role({
      name: name_val,
      label: label_val
    })
    .then(function(response) {

      if (response.fail) {

        return;

      }
      console.log(response);

      var data = response.data || {};

      data = data[0] || {
        id : 0,
        name: "",
        title: ""
      };

      var template = scope.administrate_role;

      var record = template(data);

      bind_role_events(record);

      role_aggregate.append(record);

    });

  });

  function bind_role_events (elem) {

    var role_id = elem.attr('data-role');

    var input_name = $('input.name', elem);
    var input_label = $('input.label', elem);

    // create a machine_name widget
    app.machine_name_text_field(input_name, {source: input_label});

    var field_name = $('span.field.name', elem);

    var field_label = $('span.field.label', elem);

    var edit = $('div.button.edit', elem);

    var save = $('div.button.save', elem);

    edit.click(function () {

      $(this).hide();
      field_name.hide();
      field_label.hide();
      input_name.show();
      input_label.show();
      save.show();

    });

    save.click(function () {

      $(this).hide();
      input_name.hide();
      input_label.hide();
      field_name.show();
      field_label.show();
      edit.show();

      name_val = input_name.val();
      label_val = input_label.val();

      update_role({
        role_id: role_id,
        name: name_val,
        label: label_val,
      })
      .then(function (response) {

        response = response || {};

        var data = response.data || [];

        var record = data.pop() || {};

        var name = record.name;

        field_name.text(name);

        var label = record.label;

        field_label.text(label);
      
      });
      
    }); 

    var destroy = $('div.button.destroy', elem);

    destroy.click(function () {

      var confirm_destroy = function () {

        destroy_role({role_id: role_id}).
        then(function (response) {
          
          // remove element on success
          response.success && elem.remove();

        });

      };

      var modal = scope.modal_dialogue({
        message: 'Are you sure you want to delete this role?', 
        confirm_callback: confirm_destroy, 
        cancel_callback: function () {},
      });

      $(document.body).append(modal);

    });

  }

  read_all_roles()  
  .then(function (response) {

    var template = scope.administrate_role;

    scope.apply_records({
      aggregate: role_aggregate,
      factory: template,
      records: response.data,
      bind_events_callback: bind_role_events
    });

  });

});
