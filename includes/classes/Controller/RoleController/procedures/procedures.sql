USE cms;

DROP PROCEDURE IF EXISTS create_role;
DROP PROCEDURE IF EXISTS update_role;
DROP PROCEDURE IF EXISTS destroy_role;
DROP PROCEDURE IF EXISTS grant_role_permission;
DROP PROCEDURE IF EXISTS revoke_role_permission;

DELIMITER //

CREATE PROCEDURE create_role (
  IN _name varchar(60),
  IN _label varchar(60)
)
BEGIN

  DECLARE _role_id INT;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO role (name, label)
    VALUES (_name, _label);

    SET _role_id = LAST_INSERT_ID();

    SELECT _role_id AS id, _name AS name, _label AS label;

  COMMIT;

END//

CREATE PROCEDURE update_role (
  IN _role_id SMALLINT(5),
  IN _name varchar(60),
  IN _label varchar(60)
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE role SET
    name = _name,
    label = _label
    WHERE id = _role_id;

    SELECT id, name, label FROM role WHERE id = _role_id;

  COMMIT;

END//

CREATE PROCEDURE destroy_role (
  IN _role_id SMALLINT(5)
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    DELETE FROM role
    WHERE id = _role_id;

    SELECT 1 AS success;

  COMMIT;

END//

CREATE PROCEDURE grant_role_permission (
  IN _role_id SMALLINT(5),
  IN _permission_id INT
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO role_permission (role_id, permission_id)
    VALUES (_role_id, _permission_id);

  COMMIT;

END//

CREATE PROCEDURE revoke_role_permission (
  IN _role_id SMALLINT(5),
  IN _permission_id SMALLINT(5)
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    DELETE FROM role_permission
    WHERE role_id = _role_id
    AND permission_id = _permission_id;

  COMMIT;

END//
