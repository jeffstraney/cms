<?php
namespace Jasper\Controller;

class RoleController extends ResourceController {

 protected $create_query;
 protected $read_query;
 protected $read_all_query;
 protected $update_query;
 protected $destroy_query;
 protected $role_permission_create_query;
 protected $role_permision_destroy_query;

 public function __construct () {

   parent::__construct();

   include get_entity_partial('queries');

   $this->create_query = $create_query;
   $this->read_query = $read_query;
   $this->read_all_query = $read_all_query;
   $this->update_query = $update_query;
   $this->destroy_query = $destroy_query;

   $this->role_permission_create_query = $role_permission_create_query;
   $this->role_permission_destroy_query = $role_permission_destroy_query;

 }
  
 // append resource to database
 public function create ($params = array()) {
 
   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('create_role');

   if (!$can) {

     json_response('fail', TRUE);

   }

   $name = get_val($params, 'name', 0);

   $label = get_val($params, 'label', 0);

   $query = $this->create_query;

   $result = $this->resource_query($query, array(
     ':name' => $name,
     ':label' => $label,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('data', $result);

 }

 // read resource from database
 public function read ($params = array()) {

   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('read_role');

   if (!$can) {

     json_response('fail', TRUE);

   }

   $query = $this->read_query;

   $role_id = get_val($params, 'role_id', 0);

   $result = $this->resource_query($query, array(
     ':role_id' => $role_id,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('data', $result);

 }

 public function read_all ($params = array()) {

   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('read_every_role');

   if (!$can) {

     json_response('fail', TRUE);

   }

   $query = $this->read_all_query;

   $role_id = get_val($params, 'role_id', 0);

   $result = $this->resource_query($query, array());

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('data', $result);

 }

 // updates the resource 
 public function update ($params = array()) {

   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('update_role');

   if (!$can) {

     json_response('fail', TRUE);

   }

   $query = $this->update_query;

   $role_id = get_val($params, 'role_id', 0);

   $name = get_val($params, 'name', 0);

   $label = get_val($params, 'label', 0);

   $result = $this->resource_query($query, array(
     ":role_id" => $role_id,
     ":name" => $name,
     ":label" => $label,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('data', $result);

 }

 // destroys the resource 
 public function destroy ($params = array()) {

   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }
   
   $can = $this->check_permission('destroy_role');

   if (!$can) {

     json_response('fail', TRUE);

   }
 
   $query = $this->destroy_query;

   $role_id = get_val($params, 'role_id', 0);

   $result = $this->resource_query($query, array(
     ":role_id" => $role_id,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('success', TRUE);

 }

 public function role_permission_create ($params = array()) {

   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('update_role');

   if (!$can) {

     json_response('fail', TRUE);

   }
 
   $role_id = get_val($params, 'role_id', 0);

   $permission_id = get_val($params, 'permission_id', 0);

   $query = $this->role_permission_create_query;

   $result = $this->resource_query($query, array(
     ":role_id" => $role_id,
     ":permission_id" => $permission_id,
   ));
   
 }

 public function role_permission_destroy ($params = array()) {
   
   $csrf_token = $params['csrf_token_auth'];

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $can = $this->check_permission('update_role');

   if (!$can) {

     json_response('fail', TRUE);

   }
   
   $role_id = get_val($params, 'role_id', 0);

   $permission_id = get_val($params, 'permission_id', 0);

   $query = $this->role_permission_destroy_query;

   $result = $this->resource_query($query, array(
     ":role_id" => $role_id,
     ":permission_id" => $permission_id,
   ));
   
 }

 public function administrate ($params) {

   global $configs;

   $site_root = $configs['site_root'];

   $can = $this->check_permission('administrate_role');

   if (!$can) {

     go_home();

   }

   $js_template = $this->get_js($this, 'template');

   $js_controller = $this->get_js($this, 'administrate');

   include get_entity_partial('administrate');

   return $template;

 }

 public function get_action_title ($action, $params, $titles = array()) {

   $titles = array(
     'administrate' => 'Administrate Roles',
   );

 }

}

?>
