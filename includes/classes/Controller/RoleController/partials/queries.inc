<?php
$create_query = <<<EOT
CALL create_role(:name, :label);
EOT;

$read_query = <<<EOT
SELECT id, name, label
FROM role
WHERE id = :role_id;
EOT;

$read_all_query = <<<EOT
SELECT id, name, label
FROM role;
EOT;

$update_query = <<<EOT
CALL update_role(:role_id, :name, :label);
EOT;

$destroy_query = <<<EOT
CALL destroy_role(:role_id);
EOT;

$role_permission_create_query = <<<EOT
CALL grant_role_permission(:role_id, :permission_id);
EOT;

$role_permission_destroy_query = <<<EOT
CALL revoke_role_permission(:role_id, :permission_id);
EOT;
?>
