<?php

$template = <<<EOT
<div id="inner-content">

  <form id="create-role" method="POST" action="{$site_root}/role/create">

    <label for="label">Role Label</label>
    <input class="label" type="text" name="label" placeholder="Label"/>

    <label for="name">Role Name</label>
    <input class="name" type="text" name="name" placeholder="Name"/>

    <p class="form-prompt">
      For the role's label, choose the name as you'd like it to display on the website.
      the role name must be underscore delimited and should be based on the label. Names
      are referenced by php code and by queries, while labels are present to your site user. 
    </p>

    <!-- form is AJAX-ified. CRSRF token is included via Javascript -->
    <input type="submit" value="Create Role">

  </form>

  <div class="administrate-role aggregate roles">
  
  </div>

</div>

{$js_template}

{$js_controller}

EOT;
?>
