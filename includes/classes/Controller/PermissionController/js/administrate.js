app.controller(function (scope) {

  var api_endpoint = scope.api_endpoint;

  var read_all_permissions = function (params) {

    return api_endpoint(params,{
      url: "/permission/read_all"
    });

  };

  var read_permission = function (params) {

    return api_endpoint(params,{
      url: "/permission/read"
    });

  };

  var update_permission = function (params) {

    return api_endpoint(params, {
      url: "/permission/update"
    });

  };

  var create_permission = function (params) {

    return api_endpoint(params, {
      url: "/permission/create"
    });

  };

  var destroy_permission = function (params) {

    return api_endpoint(params, {
      url: "/permission/destroy"
    });

  };

  var read_permission_roles = function (params) {

    return api_endpoint(params, {
      url: "/permission/roles"
    });

  };

  var create_role_permission = function (params) {
    return api_endpoint(params, {
      url: "/role/permission/create"
    }); 
  };

  var destroy_role_permission = function (params) {
    return api_endpoint(params, {
      url: "/role/permission/destroy"
    }); 
  };


  var permission_aggregate = $('div.permission-make.aggregate.permission');

  read_all_permissions().then(function(response) {

    response = response || {};

    var data = response.data || [];

    var template = scope.permission_template;

    scope.apply_records({
      records: data,
      factory: template,
      aggregate: permission_aggregate,
      bind_events_callback: bind_permission_events 
    });

  });


  var permission_create_form = $('form#permission-create');

  var permission_label = $('input#permission-label', permission_create_form);

  var permission_name = $('input#permission-name', permission_create_form);

  app.machine_name_text_field(permission_name, {
    source: permission_label
  });

  permission_create_form.submit(function (e) {

    e.preventDefault();

    var self = $(this);

    create_permission({

      name: permission_name.val(),

      label: permission_label.val() 

    }).
    then(function (response) {

      response = response || {}; 

      console.log(response);

      var data = response.data || [];

      var record = data.pop() || {};

      var template = scope.permission_template;

      var elem = template(record);

      bind_permission_events(elem);

      permission_aggregate.append(elem);
      
    });

    permission_label.val("");

    permission_name.val("");

  });

  // callback to destroy a permission 
  function drop_permission() {

    var self = $(this);

    var permission_id = self.parent().attr('data-permission');

    var confirm_destroy = function () {
      destroy_permission({id: permission_id})
      .then(function (response) {

        if (!response.fail) {

          self.parent().remove();

        }

      });
    }

    var modal = scope.modal_dialogue({

      message: "Are you sure you want to delete this permission?",
      // destroy the permission when 'confirm' is clicked.
      confirm_callback: confirm_destroy,
      // cancel just closes the dialogue
      cancel_callback: function () {}

    });

    $(document.body).append(modal);

  };

  function bind_permission_events(elem) {

    $('.button.destroy', elem).click(drop_permission);

    var id = elem.attr('data-permission');

    var edit = $('.button.edit', elem);

    var save = $('.button.save', elem);

    var form = $('.permission.edit.form', elem);

    var field_label = $('span.field.label', elem);
    var field_name = $('span.field.name', elem);

    var input_label = $('input.label', form);
    var input_name = $('input.name', form);

    var roles_aggregate = $('div.aggregate.roles', form);

    app.machine_name_text_field(input_name, {
      source: input_label  
    });

    var admin_role_permission = scope.admin_role_permission;

    var bind_role_permission_callback = function (elem) {

    };

    var bind_role_permission_callback = function (elem) {

      var role_id = elem.attr('data-role');


      var toggle = $('input.can', elem);

      toggle.change(function (e) {

        var can = this.checked? true: false;

        if (can) {

          create_role_permission({permission_id: id, role_id: role_id}).
          then(function (response) {

          });

        }
        else {

          destroy_role_permission({permission_id: id, role_id: role_id}).
          then(function (response) {

          });

        }

      });

    };

    edit.click(function () {

      // hide edit, show save and form
      edit.hide();

      // request the roles if they do not exist.
      if (roles_aggregate.html() == "") {

        read_permission_roles({permission_id: id})
        .then(function (response) {

          response = response || {};

          data = response.data || [];

          scope.apply_records({
            aggregate: roles_aggregate,
            factory: admin_role_permission,
            records: data,
            bind_events_callback: bind_role_permission_callback
          });
        });

      }

      // always show the form.
      form.show();
      
    });
    
    save.click(function () {

      var label = input_label.val();
      var name = input_name.val();

      update_permission({
        permission_id: id,
        label: label,
        name: name,
      })
      .then(function (response) {

        response = response || {};

        if (response.fail) {

          return;

        }

        // hide form, show edit
        edit.show();

        form.hide();

        //update text
        field_label.text(label);

        field_name.text(name);


      });
      
    });

  }

  $('div.record.permission').each(function (index, elem) {

    bindEventsPermission($(elem));

  });

});
