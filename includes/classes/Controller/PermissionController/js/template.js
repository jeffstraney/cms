// the template method attaches this factory function under
// the provided name. All template names need to be unique (per
// page requested)
app.template('permission_template', function (record) {

  var id, name, label;

  id = record.id || 0;
  name = record.name || "";
  label = record.label || "";

  // first step is to initialize the template in javascript
  var elem = $('\
  <div class="record permission" data-permission="'+id+'">\
    <span class="field-label label">Label: </span><span class="field label">' + label + '</span>\
    <span class="field-label name">Name: </span><span class="field name">'+ name +'</span>\
    <div style="display:none" class="permission edit form">\
      <label for="label">Label</label>\
      <input type="text" class="label" name="label" value="' + label + '"/>\
      <label for="name">Name</label>\
      <input type="text" class="name" name="name" value="'+ name +'"/>\
      <h4>Roles</h4>\
      <div class="permission-edit aggregate roles"></div>\
      <div class="button save">Save</div>\
    </div>\
    <div class="button destroy">X</div>\
    <div class="button edit">Edit</div>\
  </div>');

  // return template
  return elem;

});

// used to check off roles that have a permission. used when editing a permission
app.template('admin_role_permission', function (record) {

  var id = record.id;
  var label = record.label;
  var has_permission = record.has_permission? 'checked="checked"' : '';

  var elem = $('\
    <div data-role="'+id+'" class="record role">\
    <span>' + label + '</span>\
    <input class="can" type="checkbox" ' + has_permission + '/>\
    </div>\
  ');

  return elem;

});
