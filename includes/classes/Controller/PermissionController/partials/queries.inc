<?php
$create_query = <<<EOT
CALL create_permission(:name, :label);
EOT;

$update_query = <<<EOT
CALL update_permission(:permission_id, :label, :name);
EOT;

$destroy_query = <<<EOT
CALL destroy_permission(:permission_id);
EOT;

$read_query = <<<EOT
SELECT p.id, p.name, p.label
FROM permission p
WHERE :name = p.name;
EOT;

$read_user_permissions_query = <<<EOT
SELECT p.id, p.name, p.label
FROM permission p, role_permission rp, user_role ur
WHERE p.id = rp.permission_id
AND rp.role_id = ur.role_id
AND ur.user_id = :user_id;
EOT;

$user_has_permission_query = <<<EOT
CALL user_has_permission(:user_id, :permission_name);
EOT;

$read_all_query = <<<EOT
SELECT p.id, p.name, p.label 
FROM permission p;
EOT;

$read_permission_roles_query = <<<EOT
CALL read_permission_roles(:permission_id);
EOT;

?>
