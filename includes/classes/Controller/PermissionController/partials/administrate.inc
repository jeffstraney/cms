<?php
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
$template = <<<EOT
<div id="inner-content">

  <!-- create permission form -->
  <form id="permission-create" method="POST" 
     action="{$site_root}/permission/create">

    <label for="label">Permission Label</label>

    <input id="permission-label" type="text" name="label"/>

    <input id="permission-name" type="text" name="name"/>

    <p class="form-prompt">
      The permission label will be presented to users of your web app.
      The permission name is machine readable text (lowercase, underscore
      delimited) read by php and sql. To ensure exact behavior, you'll have 
      to check that you are checking permission names in the proper controller.
    </p>

    <input type="hidden" />

    <input type="submit" value="Add Permission"/>

  </form>

  <!-- start permission records-->
  <div class="permission-make aggregate permission">

  </div>

  <!-- javascript includes -->
  {$js_template}
  {$js_controller}

</div>
EOT;
////////////////////////////////////////////////////////////////////////////////
?>
