<?php

namespace Jasper\Controller;

class PermissionController extends ResourceController {

 protected $create_query;
 protected $update_query;

 protected $destroy_query;

 // reads a single permission matched by user->role->permission
 protected $read_query;

 // reads all permissions
 protected $read_all_query;

 protected $read_user_permissions_query;

 protected $read_permission_roles_query;

 protected $user_has_permission_query;

 public function __construct () {

   parent::__construct();

   include get_entity_partial('queries');

   // creates a permission
   $this->create_query = $create_query;

   $this->update_query = $update_query;

   // destroys a permission
   $this->destroy_query = $destroy_query;

   // reads a single permission
   $this->read_query = $read_query;

   // reads all permissions
   $this->read_all_query = $read_all_query;

   $this->read_user_permissions_query = $read_user_permissions_query ;

   $this->read_permission_roles_query = $read_permission_roles_query;

   // checks a user id against a permission
   $this->user_has_permission_query = $user_has_permission_query;

 }

 // form to create a permission
 public function administrate($params = array()) {

   global $user;

   $user_id = get_val($user, 'id', 0);

   $can = $this->check_permission("create_permission");

   global $configs;

   $site_root = $configs['site_root'];

   $csrf_token = get_csrf_token_auth();

   if ($csrf_token === FALSE || !$can) {

     go_home();

   }

   $permissions = $this->view_all($params);

   $js_controller = $this->get_js($this, 'administrate');

   $js_template = $this->get_js($this, 'template');

   include get_entity_partial('administrate');

   return $template;

 }
  
 // append permission to database
 public function create ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

      json_response('fail', TRUE);

   }

   global $user;

   $user_id = get_val($user, 'id', 0);

   $can = $this->check_permission("create_permission");

   if (!$can) {

     json_response('fail', TRUE);

   }

   $name = get_val($params, 'name', NULL);

   $label = get_val($params, 'label', NULL);

   $query = $this->create_query;

   $result = $this->resource_query($query, array(
     ":name" => $name,
     ":label" => $label,
   ));

   if ($result == FALSE) {

     json_response('fail', 'true');

   }

   json_response('data', $result);

 }

 // read permission from database. checks against user id
 public function read ($params = array()) {
   
   $user_id = get_val($params, 'user_id', 0);

   $permission_name = get_val($params, 'permission_name', '');

   $query = $this->read_query;

   $result = $this->resource_query($query, array(
     ':name' => $permission_name,
   ));

   if ($result === FALSE) {

     return FALSE;

   }

   return TRUE;

 }

 public function read_all ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

      json_response('fail', TRUE);

   }

   global $user;

   $user_id = get_val($user, 'id', 0);

   $has_permission = $this->read(array(
     "user_id" => $user_id,
     "permission_name" => "read_every_permission",
   ));

   if (!$has_permission) {

     json_response("fail", "true");

   }

   $query = $this->read_all_query;

   $result = $this->resource_query($query);

   if ($result === FALSE) {

     return FALSE;

   }

   json_response("data", $result);

 }

 public function view_all ($params = array()) {

   global $user;

   $user_id = get_val($user, 'id', 0);

   $has_permission = $this->check_permission("administrate_permission");

   if (!$has_permission) {

     return "";

   }

   $query = $this->read_all_query;

   $result = $this->resource_query($query);

   if ($result === FALSE) {

     return FALSE;

   }

   return $result;
   
 }

 // form to update a permission
 public function edit ($params = array()) {

 }

 // updates the permission 
 public function update ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

      json_response('fail', TRUE);

   }

   global $user;

   $user_id = get_val($user, 'id', 0);

   $can = $this->check_permission("update_permission");

   if (!$can) {

     json_response('fail', TRUE);

   }

   $id = get_val($params, 'permission_id', 0);
   $label = get_val($params, 'label', NULL);
   $name = get_val($params, 'name', NULL);

   $query = $this->update_query;

   $result = $this->resource_query($query, array(
     ":permission_id" => $id,
     ":label" => $label,
     ":name" => $name,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   json_response('data', $result);

 }

 // form to destroy a permission
 public function drop ($params = array()) {

 }

 // destroys the permission 
 public function destroy ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

      json_response('fail', TRUE);

   }

   global $user;

   $user_id = get_val($user, 'id', 0);

   $can = $this->check_permission("destroy_permission");

   if (!$can) {

      json_response('fail', TRUE);

   }

   $id = get_val($params, 'id', 0);

   $query = $this->destroy_query;

   $result = $this->resource_query($query, array(
     ":permission_id" => $id
   ));

   if ($result === FALSE) {

     json_response("fail", "true");

   }

   json_response("data", $result);

 }

 // reads the roles on a permission
 public function read_permission_roles ($params) {

   $permission_id = get_val($params, 'permission_id', NULL);

   $query = $this->read_permission_roles_query;

   $results = $this->resource_query($query, array(
     ":permission_id" => $permission_id,
   ));

   json_response('data', $results);
   
 }

 // loads the users permissions based on roles. allows
 // for a single read, rather than repeated reading
 public function read_user_permissions($params) {

   $user_id = get_val($params, 'user_id', 0);

   $query = $this->read_user_permissions_query;

   $results = $this->resource_query($query, array(
     ':user_id' => $user_id
   ));

   if ($results === FALSE) {

     // no permissions
     return array();

   }

   return $results;

 }

 // checks if a user has a permission
 public function user_has_permission($params) {

   $user_id = get_val($params, 'user_id', 0);

   $permission_name = get_val($params, 'permission_name', NULL);

   $query = $this->user_has_permission_query;

   $result = $this->db->prepared_query($query, array(
     ":user_id" => $user_id,
     ":permission_name" => $permission_name,
   ));

   return $result;

 }

 public function get_action_title ($action, $params, $titles = array()) {

   $titles = array(
     "administrate" => "Administrate Permissions",
   );

   return " | " . get_val($titles, $action, "");

 }

 
 protected function check_permission($permission_name) {

   global $user;

   $user_id = get_val($user, 'id', 0);

   // if empty, $result = FALSE
   $result = $this->user_has_permission(array(
     "user_id" => $user_id,
     "permission_name" => $permission_name,
   ));

   return $result !== FALSE;

 }

}

?>
