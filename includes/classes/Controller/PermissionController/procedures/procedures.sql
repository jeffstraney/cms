USE cms;

DROP PROCEDURE IF EXISTS user_has_permission;
DROP PROCEDURE IF EXISTS create_permission;
DROP PROCEDURE IF EXISTS update_permission;
DROP PROCEDURE IF EXISTS destroy_permission;
DROP PROCEDURE IF EXISTS read_permission_roles;

DELIMITER //

CREATE PROCEDURE user_has_permission(
  IN _user_id INT(10),
  IN _permission_name varchar(60)
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  SELECT u.id
  FROM `user` u, user_role ur, role_permission rp, permission p
  WHERE u.id = _user_id
  AND ur.user_id = u.id
  AND ur.role_id = rp.role_id
  AND rp.permission_id = p.id
  AND p.name = _permission_name;

END//

CREATE PROCEDURE create_permission(
  IN _permission_name varchar(60),
  IN _permission_label varchar(60)
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    INSERT INTO permission (name, label)
    VALUES (_permission_name, _permission_label);

    SELECT LAST_INSERT_ID() AS id, _permission_name AS name, _permission_label AS label;

  COMMIT;
END//

CREATE PROCEDURE update_permission(
  IN _permission_id INT,
  IN _label varchar(100),
  IN _name varchar(100) 
)
BEGIN
 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    UPDATE permission SET
    label = _label,
    name = _name
    WHERE id = _permission_id;

    SELECT _permission_id AS id;

  COMMIT;
  
END//

CREATE PROCEDURE destroy_permission(
  IN _permission_id INT
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    DELETE FROM permission 
    WHERE _permission_id = id;

    SELECT _permission_id AS id;

  COMMIT;

END//

CREATE PROCEDURE read_permission_roles (
  IN _permission_id INT
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    SELECT r.name, r.label, r.id, rp.role_id AS has_permission
    FROM role r
    LEFT JOIN role_permission rp ON r.id = rp.role_id
    AND rp.permission_id = _permission_id
    UNION
    SELECT r.name, r.label, r.id, NULL AS has_permission
    FROM role r
    WHERE NOT EXISTS (
      SELECT rp.role_id
      FROM role_permission rp
      WHERE r.id = rp.role_id
      AND rp.permission_id = _permission_id);

  COMMIT;

END//

DELIMITER ;
