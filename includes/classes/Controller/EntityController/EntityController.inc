<?php

namespace Jasper\Controller;

// an Entity is a resource with views.
abstract class EntityController extends ResourceController {

 public function __construct() {

   parent::__construct();

 }

 // four methods below return a rendered template found in partials

 // a form to make a new entity. sends request to [entity]/create
 public abstract function make ($params = array());

 // a form to view an entity. 
 public abstract function view ($params = array());

 // a form to edit a new entity. sends request to [entity]/update
 public abstract function edit ($params = array());

 // a form to drop an entity. sends request to [entity]/destroy
 public abstract function drop ($params = array());

 // overrides get_menu for resource. real straight forward CRUD menus. override for
 // fancy language, additional links, or special conditions based
 // on params (see UserController->get_menu as an example).
 protected function get_menu($params) {

   global $configs;

   $site_root = get_val($configs, 'site_root', '');

   $id = get_val($params, 'id', 0);

   $resource_name = $this->resource_name;

   $menu = array();

   $menu['view'] = array(
     'url' => "$site_root/$resource_name/$id",
     'text' => 'View',
     'permissions' => array("read_{$resource_name}"),
   );

   $menu['make'] = array(
     'url' => "$site_root/$resource_name/new",
     'text' => 'New',
     'permissions' => array("create_{$resource_name}"),
   );

   $menu['edit'] = array(
     'url' => "$site_root/$resource_name/$id/edit",
     'text' => 'Edit',
     'permissions' => array("update_{$resource_name}"),
   );

   $menu['drop'] = array(
     'url' => "$site_root/$resource_name/$id/delete",
     'text' => 'Delete',
     'permissions' => array("destroy_{$resource_name}"),
   );

   return $menu;

 }

 
 public function render_menu ($params = array(), $keys = array(), $class = "", $permissions = array()) {

   // user must have all permissions specified to view entire menu
   foreach($permissions as $permission_name) {

     if (!$this->check_permission($permission_name)) {

       return "";

     }

   }

   // (e.g. user, page)
   $resource_name = $this->resource_name;

   // this page belongs to an entity and to type <resource_name>
   $class .= " entity $resource_name";

   // pass altered parameters to parent method
   return parent::render_menu($params, $keys, $class);

 }

}

?>
