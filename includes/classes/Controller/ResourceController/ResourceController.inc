<?php
namespace Jasper\Controller;

use \ReflectionClass;

abstract class ResourceController {

 // Constants for text sanitization. All text is sanitized automatically based on a users
 // 'embed_<full, restricted>_html' permission. no permission implies no embedding can occur
 // if you want no embeding on a specific field, you must call the santize_xss helper function
 // from inside your controller methods
 const FULL_HTML = 2;
 const RESTRICT_HTML = 1;
 const NO_HTML = 0;
 
 protected $db;

 protected $permission_controller;

 protected $resource_name;

 protected function __construct () {

   $this->db = new \Jasper\Database();

   // get classname and remove namespace prefixes
   $class_name = get_class($this);
   $class_name = explode('\\', $class_name);
   $this->class_name = array_pop($class_name);

   $this->resource_name = strtolower(substr($this->class_name, 0, strpos($this->class_name, 'Controller')));

   // to prevent recursion, the Permissions Controller is prevented
   // from having its own permissions controller (the permissions
   // controller simply uses its own methods to check user access
   // for CRUD operations on permissions)
   if ($this->class_name == "PermissionController") {

     return;

   }

   $this->permission_controller = new PermissionController();

 }
 
 // append resource to database
 public abstract function create ($params = array());

 // read resource from database
 public abstract function read ($params = array());

 // updates the resource 
 public abstract function update ($params = array());

 // destroys the resource 
 public abstract function destroy ($params = array());


 // todo: consider if separate 'validator' class is necessary. consider
 // impacts then, on autoloading and cluttering of 'classes' directory.
 // consider an approach more compliant with psr-4 autoloading specs
 protected function validate_length ($value, $params) {

   // default no minimum
   $min = get_val($params, 0, 0);
   // default no maximum
   $max = get_val($params, 1, -1);

   $len = strlen($value);

   // if no max exists
   if ($max == -1) {

     return $len >= $min;

   }

   return $len >= $min && $len <= $max;

 }

 // returns the default parameter values to perform actions. for example,
 // if username is required, the $config array would have a key of 'username'
 // and a value of array(':username', NULL) for that key. This way, the NULL value
 // can be checked when the query is performed against a NOT NULL constraint.
 
 // alternatively, the params (sent at request time) can be used to create variable
 // default values (example, based on several provided parameters, the default value
 // of some proceding parameter changes)
 protected function param_defaults($params = array()) {

   $defaults = array();

   // override and put logic here. use $params if necessary
   $defaults['my_field'] = array(
     // an array of method names on the class. get called in sequence. each
     // method accepts the users entered value, and returns TRUE if it is valid
     // and FALSE if it is invalid. this example calls $this->validate_length with
     // min length of 0 and max of 10 
     'validators' => array('validate_length', array(0, 10)),
     // key used in prepared queries
     'prepared_key' => ':my_field',
     // default value if value is blank, empty or fails validation
     'default' => NULL, 
   );

   return $defaults;

 }

 protected function check_params ($params) {

   // array of actual values found, keyed under named prepared statement
   // placeholders. (e.g. array(":username" => NULL))
   $results = array();

   $defaults = $this->param_defaults($params); 

   foreach($defaults as $key => $default) {

     $prepared_key = get_val($default, 'prepared_key', NULL);

     $default_value = get_val_ifset($default, 'default', NULL);

     $result_value = get_val($params, $key, $default_value);

     $validators = get_val($default, 'validators', array());

     foreach ($validators as $validator => $validate_params) {

       // todo: allow exception handling.
       // call the controllers validation method
       if ($this->{$validator}($result_value, $validate_params) === FALSE) {

         return FALSE;

       }

     }

     $validated_value = $result_value;

     $results[$prepared_key] = $validated_value;

   }

   return $results;

 }

 // out of the box solution takes parameters, checks them against
 // defaults, create prepared arguments and then performs the query
 protected function resource_action($params, $action_query) {

   // get prepared arguments based on defaults. example, array(":username" => NULL)
   $prepared_arguments = $this->check_params($params);

   // use those arguments paired with the query given to this method.
   $results = $this->resource_query($action_query, $prepared_arguments);

   return $results;

 }

 // resources are concerned mainly with the current users level of access
 // this method simply checks the current users permissions array.
 protected function check_permission($permission_name) {

   global $user;

   $permissions = get_val($user, 'permissions', array());

   return isset($permissions[$permission_name])? TRUE: FALSE;

 }

 // check both csrf tokens
 protected  function check_csrf_tokens ($csrf_anon, $csrf_auth) {

   $has_csrf_anon = check_csrf_token_anon();

   $has_csrf_auth = check_csrf_token_auth();

   return $has_csrf_anon && $has_csrf_auth;

 }

 // checks the CSRF token for anonymous users, used for things like
 // logging in, signing up. form actions that an anonymous user can do.
 protected function check_csrf_token_anon ($request_token) {

   $session_token = get_val($_SESSION, 'csrf_token_anon', NULL);

   if ($request_token === $session_token) {

     return TRUE;

   }

   return FALSE;

 }

 // checks the CSRF token for authenticated users. used for priveledged
 // actions like creating a resource, deleting anything
 protected function check_csrf_token_auth ($request_token) {

   $session_token = get_val($_SESSION, 'csrf_token_auth', NULL);

   if ($request_token === $session_token) {

     return TRUE;

   }

   return FALSE;

 }

 // sanitizes parameters for XSS attacks (javascript), allows
 // 3 different modes for sanitization depending on role permissions.
 protected function sanitize_query_params (&$params = array()) {

   // by default, allow no html tags to be used
   $sanitization = $this::NO_HTML;

   // if user can use full html, allow them (give to trusted roles only)
   if ($this->check_permission('embed_full_html')) {

     $sanitization = $this::FULL_HTML;

   }
   // if user can use restricted tags, allow restricted tags specified
   // in settings.inc under $configs['xss_white_list']
   else if ($this->check_permission('embed_restricted_html')) {

     $sanitization = $this::RESTRICT_HTML;

   }

   foreach($params as $key => $value) {

     $params[$key] = sanitize_xss($value, $sanitization);

   }

 }

 // takes the pain out of getting results. After enough controllers were
 // made, I realized I was repeating a lot of code. This is a DRY-er solution
 protected function resource_query($query, $params = array()) {

   $this->sanitize_query_params($params);

   if (empty($params)) {

     $results = $this->db->query($query);

   }
   else {

     $results = $this->db->prepared_query($query, $params);

   }

   if (empty($results)) {

     return FALSE;

   }
   else if ($error = get_val($results, 'error', NULL)) {

     set_message($error, 'error');

     // redirect handled by controller
     return FALSE;

   }

   return $results;

 }

 // gets the directory of child directory todo: find a cleaner way
 // to do this without injecting $this as parameter
 protected function get_child_class_dir ($self) {

   $class = new ReflectionClass($self);

   return dirname($class->getFileName());

 }

 // checks to see if the partial exists for the class
 protected function partial_exists ($self, $partial) {

   $dir = $this->get_child_class_dir($self);

   if (file_exists($dir . '/partials/' . $partial . '.inc')) {

     return TRUE;

   }

   return FALSE;

 }

 // todo: see if there is a cleaner way to reference child class's
 // directory to include javascript relative to child class
 protected function get_js ($self, $js_file) {

   $dir = ($this->get_child_class_dir($self));

   $js_file = $dir . '/js/' . $js_file . '.js';

   return '<script>' . file_get_contents($js_file) . '</script>';

 }

 // looks at the action decided by Router. can additionally
 // use params to effect subtitle
 public function get_action_title ($action, $params, $titles = array()) {

   $title = get_val($titles, $action, "");

   $title = $title == "" ? "": " | " . $title;

   return $title;

 }

 // uses the action chosen by the router and parameters passed to this controller to
 // apply css classes to the top level container of the view. The classes and ids
 // arrays are keyed by action
 public function get_action_css_class ($action, $params, $classes = array()) {

   return get_val($classes, $action, '');
  
 }

 // returns an array of menus
 // each controller may override build_menu to create their own menus
 protected function get_menu ($params) {

   // it is your responsibility to include the site root if you are
   // overriding this method. This is because you may want links to
   // another domain (e.g. https://www.google.com ).
   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $menu = array();

   // example menu link. can only be printed with sufficient
   // permission.
   $menu['new'] = array(
     'url' => $site_root . '/resource/new',
     'text' => 'Create Resource',
     'permission' => 'create_resource',
   );

   return $menu;

 }

 public function render_menu ($params = array(), $keys = array(), $class = "", $permissions = array()) {

   // check all permissions associated with this menu, return if any fail
   foreach ($permissions as $permission) {

     if (!$this->check_permission($permission)) {

       return "";

     }

   }

   // get all menu items for this resource
   $menu_items = $this->get_menu($params);

   // menu is simply an unordered list with anchor tags 
   $menu = '<ul class="menu ' . $class . '">';

   // render permissible links in the menu
   foreach($keys as $key) {

     // checks the permissions and renders linked li tags
     $menu .= $this->render_menu_item($key, $menu_items, $params);

   }

   // close menu
   $menu .= '</ul>';

   // return rendered text
   return $menu;

 }

 // takes the key of the item (keyed in get_menu) as
 // well as the array of menu items.
 public function render_menu_item ($key, $menu_items, $params = array()) {

   // get the menu item, as well as its url, its
   // text, and the permission associated with the link
   $item = $menu_items[$key]; 

   $url = get_val($item, 'url', '');

   $text = get_val($item, 'text', '');

   $permissions = get_val($item, 'permissions', array());

   foreach ($permissions as $permission) {

     if (!$this->check_permission($permission)) {

       // return empty string on insufficient permissions.
       return "";

     }

   }

   // alternatively, callbacks may be used to decide whether a link renders or not.
   // must be a method on the controller class. should return TRUE or FALSE
   $permission_callbacks = get_val($item, 'permission_callbacks', array());

   foreach ($permission_callbacks as $callback) {

     // call callback on parameters
     if (!$this->{$callback}($params)) {

       // return empty string on insufficient permissions.
       return "";

     }

   }

   // render the link only if the current user has all permissions
   return "<li><a href=\"{$url}\">{$text}</a></li>";

 }

}

?>
