<?php

$csrf_token = get_csrf_token_anon();

$template = <<<EOT
<div id="inner-content">

  <form id="session-make" method="POST" action="{$site_root}/session/create">

    <h2>Sign In</h2>

    <label for="credentials">Username or Email</label>
    <input id="credentials" type="text" name="credentials"  />

    <label for="password">Password</label>
    <input id="password" type="password" name="password"/>

    <input type="hidden" name="csrf_token_anon" value="{$csrf_token}"/>

    <input type="submit" value="Sign In" />

  </form>

</div>
EOT;
?>
