<?php

namespace Jasper\Controller;

class SessionController extends ResourceController {
 
 protected $create_query;
 protected $credentials_query;
 protected $record_login_query;
 protected $destroy_query;

 public function __construct () {

   parent::__construct();

   include get_entity_partial('queries');

   $this->credentials_query = $credentials_query;
   $this->record_login_query = $record_login_query;

 }
  
 // take credentials and password from user form 
 public function create ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_anon', NULL);

   if (!$this->check_csrf_token_anon($csrf_token)) {

     json_response('fail', TRUE);

   }

   $credentials = get_val($params, 'credentials', NULL);

   $password = get_val($params, 'password', NULL);

   $query = $this->credentials_query;

   $result = $this->db->prepared_query($query, array(
     ':credentials' => $credentials,
   ));

   if (empty($result)) {

     // log json and die
     json_response("fail", "Incorrect log in information.");

   }

   $result = $result[0];

   $hash = $result['hash'];

   if (!check_password($password, $hash)) {

     // log json and die
     json_response("fail", "Incorrect log in information.");

   }

   // set session user id 
   $id = $result['id'];

   $_SESSION['id'] = $id;

   // set csrf token for secure forms.
   $_SESSION['csrf_token_auth'] = bin2hex(random_bytes(32));

   // now update the timestamp for login
   $query = $this->record_login_query;

   $this->resource_query($query, array(
     ':user_id' => $id,
   ));

   redirect_entity_view('user', $id);

 }

 // read resource from database
 public function read ($params = array()) {

   // empty
   
 }

 // updates the resource 
 public function update ($params = array()) {

   // empty
   
 }

 // destroys the resource, or logs the user out 
 public function destroy ($params = array()) {

   // parameters are actually passed from the drop method
   // this is because destroy is conventionally a POST action and drop is
   // a GET action (for a form). Loging out is done through get request.
   $csrf_token_auth = get_val($params, 1, 0);

   // check the submitted token from GET request against the session
   if (!$this->check_csrf_token_auth($csrf_token_auth)) {

      // return if it is no good.
      return;

   }

   global $user;

   if ($user == NULL) {

     return;

   } 

   session_unset();

   session_destroy();

 }

 // provides the log in form
 public function make($params = array()) {

   global $configs;

   $site_root = $configs['site_root'];

   include get_entity_partial('make');

   return $template;

 }

 // no log-out form, just perform destroy, go home
 public function drop ($params = array()) {
   
   $this->destroy($params);

   go_home();
 
 }



}

?>
