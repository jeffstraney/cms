USE cms;

DROP PROCEDURE IF EXISTS get_hash;
DROP PROCEDURE IF EXISTS record_login;

DELIMITER //

CREATE PROCEDURE get_hash(
  IN _credential varchar(60) 
)
BEGIN
  SELECT u.id, u.hash
  FROM `user` u
  WHERE u.username = _credential OR u.email = _credential;
END//

CREATE PROCEDURE record_login (
  IN _user_id INT(10)
)
BEGIN
  START TRANSACTION;

    UPDATE `user` SET
    last_login = NOW()
    WHERE id = _user_id;

  COMMIT;

END//

DELIMITER ;
