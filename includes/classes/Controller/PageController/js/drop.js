app.controller(function (scope) {

  var api_endpoint = scope.api_endpoint;

  var destroy_page = function (params) {

    return api_endpoint(params, {
      url: "/page/destroy"
    });

  };

  $(function () {

    var form = $('form#page-drop');

    console.log(scope);

    form.submit(function (e) {

      e.preventDefault();

      var self = $(this);
      var id = $('input.id', self).val();

      function confirm_destroy () {

        destroy_page({
          id: id
        })
        .then(function (response) {

          console.log(response);

          if (response.fail) {

            return;

          }

          var user = scope.user;
          var user_id = user.id;

          // go to account page
          window.location = app.site_root + '/user/' + user_id;

        });

      }

      // dialogue performs destroy action on confirm
      var dialogue = scope.modal_dialogue({

        message: "Please Confirm",
        confirm_callback: confirm_destroy,
        confirm_text: "Delete Page",
        cancel_callback: function () {

        },
        cancel_text: "Close",

      });

      $('body').append(dialogue);

    });

  });

});
