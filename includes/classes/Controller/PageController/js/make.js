app.controller(function (scope) {

  var api_endpoint = scope.api_endpoint;

  var create_page = function (params) {

    return api_endpoint(params, {
      url: "/page/create"
    });

  };

  $(function () {

    /* todo: add code editor functionality
    var text_area = $('textarea.body')[0];

    var code_mirror = CodeMirror.fromTextArea(text_area, {
      mode: "text/html",  
      lineNumbers: true,
    });
    */

    var form = $('form#page-make');

    form.submit(function (e) {

      e.preventDefault();

      var self = $(this);
      var title = $('input.title', self).val();
      var body = $('textarea.body', self).val();
      var restricted = $('input.restricted', self)[0].checked;
      var published = $('input.published', self)[0].checked;
      var path = $('input.path', self).val();

      create_page({
        title: title, 
        body: body, 
        restricted: restricted, 
        published: published, 
        path: path, 
      })
      .then(function (response) {

        console.log(response);

        if (response.fail) {

          return;

        }

        var data = response.data || {};

        var id = data.id || 0;

        var dialogue = scope.modal_dialogue({

          message: "Your page has been created",

          confirm_callback: function () {

            window.location = app.site_root + '/page/' + id;

          },
          confirm_text: "See Page",
          cancel_callback: function () {

          },
          cancel_text: "Close",
        });

        $('body').append(dialogue);

      });

    });

  });

});
