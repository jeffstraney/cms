<?php
namespace Jasper\Controller;

// The page controller is used to create basic, static informational pages
// with little or no dynamic server-side content. Consider for example, an
// 'about' page or a 'contact us' page. These pages are stored in the database
// can be given custom paths, and have access settings which respond to
// permissions
class PageController extends EntityController {

 protected $create_query;
 protected $update_query;
 protected $destroy_query;
 protected $view_query;

 public function __construct () {

   parent::__construct();

   include get_entity_partial('queries');

   $this->create_query = $create_query;

   $this->update_query = $update_query;

   $this->destroy_query = $destroy_query;

   $this->view_query = $view_query;

 }

 // CRUD
 public function create ($params = array()) {

   global $user;

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   if (!$this->check_permission('create_page')) {

     json_response('fail', TRUE);

   }

   $title = get_val($params, 'title', NULL);
   $body = get_val($params, 'body', NULL);
   $restricted = get_val($params, 'restricted', FALSE) == 'true'? TRUE: FALSE;
   $published = get_val($params, 'published', FALSE) == 'true'? TRUE: FALSE;
   $path = get_val($params, 'path', NULL);
   $author_id = get_val($user, 'id', 0);

   $query = $this->create_query;

   $result = $this->resource_query($query, array(
     ':title' => $title,
     ':body' => $body,
     ':restricted' => $restricted,
     ':published' => $published,
     ':path' => $path,
     ':author_id' => $author_id,
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   $result = $result[0];

   json_response('data', $result);

 }

 public function read ($params = array()) {

 }

 public function update ($params = array()) {

   global $user;

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   if (!$this->check_permission('update_page')) {

     json_response('fail', TRUE);

   }

   $id = get_val($params, 'id', 0);
   $title = get_val($params, 'title', NULL);
   $body = get_val($params, 'body', NULL);
   $restricted = get_val($params, 'restricted', FALSE) == 'true'? TRUE: FALSE;
   $published = get_val($params, 'published', FALSE) == 'true'? TRUE: FALSE;
   $path = get_val($params, 'path', NULL);
   $author_id = get_val($user, 'id', 0);

   $query = $this->update_query;

   $result = $this->resource_query($query, array(
     ':id' => $id,
     ':title' => $title,
     ':body' => $body,
     ':restricted' => $restricted,
     ':published' => $published,
     ':path' => $path,
     ':author_id' => $author_id,  
   ));

   if ($result === FALSE) {

     json_response('fail', TRUE);

   }

   $result = $result[0];

   json_response('data', $result);

 }

 public function destroy ($params = array()) {

   global $user;

   $user_id = get_val($user, 'id', 0);

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   if (!$this->check_permission('destroy_page')) {

     json_response('fail', TRUE);

   }

   $id = get_val($params, 'id', 0);

   $query = $this->destroy_query;

   $result = $this->resource_query($query, array(
     ':id' => $id,
   ));

   if ($result === FALSE) {

   }

   $result = $result[0];

   set_message('You have successfully deleted your page.');

   json_response('data', $result);

 }

 // four methods below return a rendered template found in partials
 // a form to make a new entity. sends request to [entity]/create
 public function make ($params = array()) {

   if (!$this->check_permission('create_page')) {

     go_home();

   }

   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $csrf_token = get_csrf_token_auth();

   $js_controller = $this->get_js($this, 'make');

   include get_entity_partial('make');

   return $template;

 }

 // a form to view an entity. 
 // In the instance of pages, this one is a little interesting. Basically,
 // the router class will match * last. whatever is matched is attempted
 // to be found as a partial by this controller, if it cannot find the
 // partial, it returns a '404' partial
 public function view ($params = array()) {

   // anonymous users can view pages, simply add permission checks here to
   // restrict access.
   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $csrf_token = get_csrf_token_auth();

   // since page access can be done by id or page path, we have to check
   // the parameters passed to this action to determine proper controller
   // handling.
   $by_id = get_val($params, 0, FALSE) == "page"? TRUE: FALSE;

   if ($by_id) {
     $id = get_val($params, 1, 1);
     $path = NULL;
   }
   else {
     $id = NULL;
     $path = get_val($params, 0, 'home');
   }

   $query = $this->view_query;

   // get page by title or id.
   $result = $this->resource_query($query, array(
     ':id' => $id,
     ':path' => $path,
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }
   $result = $result[0];

   // get id from result in case path was used in query
   $id = get_val($result, 'id', $id);

   // set template variables 
   $title = get_val($result, 'title', '');
   $body = get_val($result, 'body', '');
   $restricted = get_val($result, 'restricted', 0) == 1? TRUE: FALSE;
   $published = get_val($result, 'published', 0) == 1? TRUE: FALSE;

   // if the page is restricted and user cannot see restricted pages, return a 404
   if ($restricted && !$this->check_permission('read_restricted_page')) {

     include get_entity_partial('404');

     return $template;

   }
   
   // if the page is is not published and user does not administrate
   // pages, return a 404.
   if (!$published && !$this->check_permission('administrate_page')) {

     include get_entity_partial('404');

     return $template;

   }

   // render menu
   $menu = $this->render_menu(
     array("id" => $id),
     array('edit', 'drop', 'make'),
     "",
     array('administrate_page')
   );

   // include template
   include get_entity_partial('view');

   // return template
   return $template;

 }

 // a form to edit a new entity. sends request to [entity]/update
 public function edit ($params = array()) {

   // if you can't update a page, you shouldn't see this form.
   if (!$this->check_permission('update_page')) {

     go_home();

   }

   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $csrf_token = get_csrf_token_auth();

   // since page access can be done by id or page path, we have to check
   // the parameters passed to this action to determine proper controller
   // handling.
   $by_id = get_val($params, 0, FALSE) == "page"? TRUE: FALSE;

   if ($by_id) {
     $id = get_val($params, 1, 1);
     $path = NULL;
   }
   else {
     $id = NULL;
     $path = get_val($params, 0, 'home');
   }

   $query = $this->view_query;

   // get page by title or id.
   $result = $this->resource_query($query, array(
     ':id' => $id,
     ':path' => $path,
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }

   $result = $result[0];

   // set id in case path was used.
   $id = get_val($result, 'id', $id);

   // render navigation.
   $menu = $this->render_menu(
     array("id" => $id),
     array('view', 'drop', 'make'),
     "",
     array('administrate_pages')
   );

   // set defaults for form.
   $title = get_val($result, 'title', '');
   $body = get_val($result, 'body', '');
   $restricted = get_val($result, 'restricted', 0)? 'checked="checked"': '';
   $published = get_val($result, 'published', 0)? 'checked="checked"': '';
   $current_path = get_val($result, 'path', '');

   $js_controller = $this->get_js($this, 'edit');

   include get_entity_partial('edit');

   return $template;

 }

 // a form to drop an entity. sends request to [entity]/destroy
 public function drop ($params = array()) {

   if (!$this->check_permission('destroy_page')) {

     go_home();

   }

   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $csrf_token = get_csrf_token_auth();

   $page = get_val($params, 0, "home");

   if ($page == 'page') {

     $page = NULL;

   }

   $id = get_val($params, 1, NULL);

   $query = $this->view_query;

   // get page by title or id.
   $result = $this->resource_query($query, array(
     ':id' => $id,
     ':title' => $page,
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }

   $result = $result[0];

   $menu = $this->render_menu(
     array('id' => $id),
     array('view', 'edit', 'make'),
     "",
     array('administrate_pages')
   );

   $title = get_val($result, 'title', '');

   $body = get_val($result, 'body', '');

   $js_controller = $this->get_js($this, 'drop');

   include get_entity_partial('drop');

   return $template;

 }

 public function get_action_title ($action, $params, $titles = array()) {

   $page_title = get_val($params, 0, 'Home');
   $page_id = get_val($params, 1, 0);

   if ($page_title == "page" && $page_id != 0) {

     $view_title = 'View Page';

   }
   else {

     $view_title = ucfirst(implode(' ', explode('-', $page_title)));

   }

   // titles for pages.
   $titles['make'] = 'New Page';
   $titles['view'] = $view_title;
   $titles['edit'] = 'Edit ' . $view_title;
   $titles['drop'] = 'Delete Page';

   return ' | ' . get_val($titles, $action, '');

 }

 protected function param_defaults ($params = array()) {

   global $user;

   // current user should always be used as author
   $user_id = get_val($user, 'id', 0);

   // restricted and published need to be changed from strings to boolean
   $restricted = get_val($params, 'restricted', FALSE) == 'true'? TRUE: FALSE;
   $published = get_val($params, 'published', FALSE) == 'true'? TRUE: FALSE;

   $params['restricted'] = $restricted;
   $params['published'] = $published;

   $defaults = array();

   $defaults['title'] = array(
     'validators' => array(
       'validate_length' => array(0, 60),
     ),
     'prepared_key' => ':title',
     'default' => NULL,
   );

   $defaults['body'] = array(
     'validators' => array(
       'validate_length' => array(0, 500),
     ),
     'prepared_key' => ':body',
     'default' => NULL,
   );

   $defaults['id'] = array(
     'prepared_key' => ':id',
     'default' => 0,
   );

   $defaults['restricted'] = array(
     'prepared_key' => ':restricted',
     'default' => FALSE,
   );

   $defaults['published'] = array(
     'prepared_key' => ':published',
     'default' => FALSE,
   );

   $defaults['author_id'] = array(
     'prepared_key' => ':author_id',
     'default' => $user_id,
   );

   $defaults['path'] = array(
     'prepared_key' => ':path',
     'default' => NULL,
   );

   return $defaults;

 }

}

?>
