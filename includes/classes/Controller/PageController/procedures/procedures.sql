USE cms;

DROP PROCEDURE IF EXISTS create_page;
DROP PROCEDURE IF EXISTS view_page;
DROP PROCEDURE IF EXISTS update_page;

DELIMITER //

CREATE PROCEDURE create_page (
  IN _title varchar(100),
  IN _body TEXT,
  IN _restricted INT(1),
  IN _published INT(1),
  IN _path varchar(60),
  IN _author_id INT(10)
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO page (title, body, restricted, published, path, author_id)
    VALUES (_title, _body, _restricted, _published, _path, _author_id);

    SELECT LAST_INSERT_ID() AS id;

  COMMIT;

END//

CREATE PROCEDURE view_page (
  IN _id INT(10),
  IN _path varchar(60)
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    SELECT id, title, body, restricted, published, path, author_id
    FROM page
    WHERE id = _id
    OR path = _path;

  COMMIT;

END//


CREATE PROCEDURE update_page (
  IN _id INT(10),
  IN _title varchar(60),
  IN _body TEXT,
  IN _restricted INT(1),
  IN _published INT(1),
  IN _path varchar(60),
  IN _author_id INT(10)
)
BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE page SET 
    title = _title,
    body = _body,
    restricted = _restricted,
    published = _published,
    path = _path,
    author_id = _author_id
    WHERE id = _id;

    SELECT _id AS id;

  COMMIT;

END//
