<?php
$template = <<<EOT

{$menu}

<!-- codemirror dependencies for editor 
<script src="{$site_root}/js/lib/codemirror/codemirror.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/xml/xml.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/javascript/javascript.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/css/css.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link rel="stylesheet" type="text/css" href="{$site_root}/js/lib/codemirror/codemirror.css"/>
-->

<form id="page-edit" class="wide" method="POST" action="{$site_root}/page/update">
  <h2>Edit Page</h2>

  <label for="title">Title</label>
  <input class="title" name="title" type="text" value="{$title}" />

    <label for="body">Body</label>
    <textarea class="body" name="body">{$body}</textarea>

  <fieldset>

    <legend>Access Settings</legend>

    <span class="nowrap">
      <label for="restricted">Restricted</label>
      <input class="restricted" name="restricted" type="checkbox" {$restricted}
       title="invisible to users without 'view_restricted_page' permission"/>
    </span>

    <span class="nowrap">
      <label for="published">Published</label>
      <input class="published" name="published" type="checkbox" {$published}
        title="if unchecked, visible only to users without 'administrate_pages' permission"/>
    </span>

  </fieldset>

  <label for="path">Path</label>

  <p class="form-prompt">
    Enter a partial url that would follow your site's domain. keep the URL path
    lowercase and hyphen delimited.
  </p>

  <input class="path" name="path" type="text" value="{$current_path}"
    title="human readable path (e.g. promotions") />

  <input name="csrf_token_auth" type="hidden" value="{$csrf_token}" />

  <input class="id" name="id" type="hidden" value="{$id}" />

  <input type="submit" value="Save"/>

</form>

{$js_controller}

EOT;
?>
