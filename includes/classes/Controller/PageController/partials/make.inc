<?php
$template = <<<EOT

<!-- codemirror dependencies for editor 
<script src="{$site_root}/js/lib/codemirror/codemirror.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/xml/xml.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/javascript/javascript.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/css/css.js"></script>
<script src="{$site_root}/js/lib/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link rel="stylesheet" type="text/css" href="{$site_root}/js/lib/codemirror/codemirror.css"/>
-->

<form id="page-make" class="wide" method="POST" action="{$site_root}/page/create">

  <h3>Create a New Page</h3>

  <label for="title">Title</label>
  <input class="title" type="text" name="title"/>

  <label for="body">Body</label>
  <textarea class="body" name="body"></textarea>

  <fieldset>

    <legend>Access Settings</legend>

    <span class="nowrap">
      <label for="restricted">Restricted</label>
      <input class="restricted" name="restricted" type="checkbox"
       title="invisible to users without 'view_restricted_page' permission"/>
    </span>

    <span class="nowrap">
      <label for="published">Published</label>
      <input class="published" name="published" type="checkbox"
        title="if unchecked, visible only to users without 'administrate_pages' permission"/>
    </span>

  </fieldset>

  <label for="path">Path</label>

  <p class="form-prompt">
    Enter a partial url that would follow your site's domain. keep the URL path
    lowercase and hyphen delimited. All paths must be unique.
  </p>

  <input class="path" name="path" type="text" value=""
    title="human readable path (e.g. promotions") />

  <input type="hidden" name="csrf_token_auth" value="{$csrf_token}"/>

  <input type="submit" value="Create Page"/>

</form>

{$js_controller}

EOT;
?>
