<?php
$template = <<<EOT

{$menu}

<form id="page-drop" method="POST" action="{$site_root}/page/{$id}/destroy">

  <h2>Delete Page</h2>

  <p>
    Deleting a page is permanent and cannot be undone. Are you sure you want
    to delete this page?
  </p>

  <input name="csrf_token_auth" type="hidden" value="{$csrf_token}"/>

  <input class="id" name="id" type="hidden" value="{$id}"/>

  <input type="submit" value="Delete Page"/>

</form>

{$js_controller}

EOT;
?>
