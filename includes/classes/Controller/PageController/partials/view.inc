<?php
/*
<ul class="menu admin page">
  <li><a href="{$site_root}/page/{$id}/delete">Delete Page</a></li>
  <li><a href="{$site_root}/page/{$id}/edit">Edit Page</a></li>
</ul>
 */
$template = <<<EOT

{$menu}

<h2>
  {$title}
</h2>

<div class="page-body">
  {$body}
</div>

EOT;
?>
