<?php

http_response_code(404);

$template = <<<EOT

<h3>Woops!</h3>

<p>
  The page you're looking for cannot be found!
</p>

EOT;
?>
