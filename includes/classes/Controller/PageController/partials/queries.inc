<?php
$create_query = <<<EOT
CALL create_page(:title, :body, :restricted, :published, :path, :author_id);
EOT;

$update_query = <<<EOT
CALL update_page(:id, :title, :body, :restricted, :published, :path, :author_id);
EOT;

$destroy_query = <<<EOT
DELETE FROM page
WHERE id = :id;
EOT;

$view_query = <<<EOT
CALL view_page(:id, :path);
EOT;

?>
