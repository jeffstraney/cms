<?php

$csrf_token = get_csrf_token_anon();

$template = <<<EOT
  <form id="user-make" method="POST" action="{$site_root}/user/create">

    <h2>New Account</h2>

    <label for="username">User Name</label>
    <input id="username" name="username" type="text" value="" />

    <label for="email">E-mail</label>
    <input id="email" name="email" type="email" value="" />

    <label for="password">Password</label>
    <input id="password" name="password" type="password" value="" />

    <label for="confirm">Confirm Password</label>
    <input id="confirm" name="confirm" type="password" value="" />

    <input type="hidden" name="csrf-token-anon" value="{$csrf_token}"/>
    <input class="submit" value="submit" type="submit" value="" />

  </form>
EOT;

?>
