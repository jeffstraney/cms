<?php
$create_query = <<<EOT
CALL create_user(:username, :email, :hash, :auth_url);
EOT;

$auth_query = <<<EOT
CALL authenticate_user(:auth_url);
EOT;

$view_query = <<<EOT
SELECT u.id, u.username, u.email
FROM `user` u
WHERE :id = u.id;
EOT;

$update_query = <<<EOT
CALL update_user(:id, :username, :email);
EOT;

$read_query = <<<EOT

EOT;

$read_all_query = <<<EOT
SELECT u.id, u.username, u.email, u.blocked
FROM `user` u;
EOT;

$destroy_query = <<<EOT
DELETE FROM user
WHERE id = :user_id;
EOT;

$change_user_status_query = <<<EOT
UPDATE `user` SET
blocked = :blocked
WHERE id = :user_id;
EOT;

$password_reset_query = <<<EOT
UPDATE `user` SET
hash = :hash
WHERE id = :id;
EOT;

$get_current_user_query = <<<EOT
SELECT u.id, u.username, u.email
FROM `user` u
WHERE :id = u.id;
EOT;

$credential_query = <<<EOT
SELECT u.hash
FROM user u
WHERE username = :username
OR email = :email
OR id = :id;
EOT;
?>
