<?php
$template = <<<EOT

{$menu}

<form id="user-edit" method="POST" action="{$site_root}/user/update">

  <h2>Edit Account</h2>

  <input type="hidden" name="csrf_token_auth" value="{$csrf_token}"/>

  <input class="id" type="hidden" name="id" value="{$id}"/>

  <label for="username">Username</label>
  <input class="username" type="text" name="username" value="{$username}"/>

  <label for="email">E-mail</label>
  <input class="email" type="email" name="email" value="{$email}"/>

  <label for="password">Password</label>

  <p class="form-prompt">
    To save changes, enter your password
  </p>

  <input class="password" name="password" type="password" value="" />

  <input type="submit" value="Save"/>

</form>

{$low_menu}

{$js_controller}

EOT;
?>
