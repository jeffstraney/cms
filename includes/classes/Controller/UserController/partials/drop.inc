<?php
$template = <<<EOT

{$menu}

<form id="user destroy" method="POST" action="{$site_root}/user/destroy">
  <h2>Delete Account</h2>

  <p>
  Deleting your account is a permanent action. Once it is deleted, it may never be
  retrieved. Are you sure you want to delete your account?
  </p>

  <label for="password">Password</label>
  <input type="password" name="password" />

  <input type="hidden" name="csrf_token_auth" value="{$csrf_token}"/>
  <input type="hidden" name="id" value="{$id}"/>

  <input type="submit" value="Delete Account"/>

</form>
EOT;
?>
