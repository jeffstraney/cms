<?php
$template = <<<EOT

{$menu}

<form id="user-password-reset" method="POST" action="{$site_root}/user/password-reset">

  <h2>Password Reset</h2>

  <label for="password">New Password</label>
  <input class="password" name="password" type="password" value="" />

  <label for="confirm">Confirm Password</label>
  <input class="confirm" name="confirm" type="password" value="" />

  <input name="csrf_token" type="hidden" value="{$csrf_token}" />

  <input type="submit" value="Change Password"/>

</form>

{$js_controller}

EOT;
?>
