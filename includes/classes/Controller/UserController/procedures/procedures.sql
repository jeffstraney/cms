USE cms;

DROP PROCEDURE IF EXISTS create_user;
DROP PROCEDURE IF EXISTS update_user;
DROP PROCEDURE IF EXISTS authenticate_user;

DELIMITER //

CREATE PROCEDURE create_user(
  IN _username varchar(60),
  IN _email varchar(60),
  IN _hash varchar(60),
  IN _auth_url varchar(60)
)
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO temp_user (username, email, hash, auth_url)
    VALUES (_username, _email, _hash, _auth_url);

    SELECT 1 AS success;

  COMMIT;

END//

CREATE PROCEDURE update_user(
  IN _id INT(10),
  IN _username varchar(60),
  IN _email varchar(60)
)  
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE user SET
    username = _username,
    email = _email
    WHERE id = _id;

    SELECT _id AS id;

  COMMIT;

END//

CREATE PROCEDURE authenticate_user(
  IN _auth_url varchar(60)
)
BEGIN

  DECLARE _id INT(10);

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO `user` (username, email, hash)
    SELECT username, email, hash
      FROM temp_user
      WHERE auth_url = _auth_url;

    SET _id = LAST_INSERT_ID();

    -- give user default of 'authenticated user'
    INSERT INTO user_role (user_id, role_id)
    VALUES (_id, 2);

    DELETE FROM temp_user WHERE auth_url = _auth_url;

    SELECT _id AS id;

  COMMIT;

END//

DELIMITER ;
