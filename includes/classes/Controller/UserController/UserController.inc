<?php

namespace Jasper\Controller;

// an Entity is a resource with views.
class UserController extends EntityController {

 protected $view_query;

 protected $create_query;

 protected $change_user_status_query;

 protected $read_query;

 protected $update_query;

 protected $credential_query;

 protected $password_reset_query;

 protected $read_all_query;

 protected $destroy_query;

 protected $auth_query;

 protected $get_current_user_query;

 public function __construct() {

   parent::__construct();

   include get_entity_partial('queries');

   $this->view_query = $view_query;

   $this->create_query = $create_query;

   $this->read_query = $read_query;

   $this->update_query = $update_query;

   $this->credential_query = $credential_query;

   $this->change_user_status_query = $change_user_status_query;

   $this->password_reset_query = $password_reset_query;

   $this->read_all_query = $read_all_query;

   $this->destroy_query = $destroy_query;

   $this->auth_query = $auth_query;
   
   $this->get_current_user_query = $get_current_user_query;

 }

 public function make ($params = array()) {

   global $configs;

   $site_root = $configs['site_root'];

   include get_entity_partial('make');

   return $template;

 }

 public function create ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_anon', NULL);

   if (!$this->check_csrf_token_anon($csrf_token)) {

     json_response('fail', TRUE);

   }

   $username = get_val($params, 'username', NULL);
   $email = get_val($params, 'email', NULL);
   $password = get_val($params, 'password', NULL);
   $confirm = get_val($params, 'confirm', NULL);

   if ($password != $confirm) {

     json_response('fail', 'Passwords must match');

   }

   $query = $this->create_query;

   $hash = encrypt_password($password);

   $auth_url = bin2hex(random_bytes(30));

   $result = $this->db->prepared_query($query, array(
     ":username" => $username, 
     ":email" => $email, 
     ":hash" => $hash, 
     ":auth_url" => $auth_url, 
   ));

   if(empty($result)) {

     json_response('fail', 'Something went wrong creating your account.');

   }

   if ($msg = get_val($result, 'error', NULL)) {

     json_response('fail', $msg); 

   }

   $result = $result[0];

   $success = $result['success'];

   if ($success === 1) {

     set_message("You've successfully made an account! We've sent an e-mail for you to authenticate your new account.");

     go_home();

   }
   
 }

 public function view ($params = array()) {

   global $configs;

   $site_root = $configs['site_root'];

   global $user;

   $id = get_val($params, 1, 0);

   $current_user_id = get_val($user, 'id', 0);

   if ($current_user_id == $id) {

     $can = TRUE;

   }
   else {

     // check if the user can even view users (site may be configured to hide
     // profiles from anonymous users)
     $can = $this->check_permission("read_user");

   }

   if (!$can) {

     include get_entity_partial('404');

     return $template;

   }

   $query = $this->view_query;

   $result = $this->resource_query($query, array(
     ":id" => $id,
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }

   $result = $result[0];

   $menu = $this->render_menu(
     array('id' => $id),
     array('view', 'edit', 'drop')
   );

   $username = get_val($result, 'username', '');

   include get_entity_partial('view');

   return $template;

 }

 // read as json response
 public function read ($params = array()) {

 }

 // edit form
 public function edit ($params = array()) {

   global $configs;

   $site_root = $configs['site_root'];

   $id = get_val($params, 1, 0);

   $can = $this->check_permission('update_user');

   $can = $can || $this->is_current_user(array(
     "id" => $id
   ));

   if (!$can) {

     go_home();

   }

   $query = $this->view_query;

   $result = $this->resource_query($query, array(
     ":id" => $id
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }

   $result = $result[0];

   $csrf_token = get_csrf_token_auth();

   $menu = $this->render_menu(
     array('id' => $id),
     array('view')
   );

   $low_menu = $this->render_menu(
     array('id' => $id),
     array('password_reset_form', 'drop')
   );

   $username = get_val($result, 'username', '');

   $email = get_val($result, 'email', '');

   $js_controller = $this->get_js($this, 'edit');

   include get_entity_partial('edit');

   return $template;

 }

 public function update ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   $id = get_val($params, 'id', 0);

   $username = get_val($params, 'username', NULL);

   $email = get_val($params, 'email', NULL);

   $password = get_val($params, 'password', NULL);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   if (!$this->check_permission('update_user')) {

     json_response('fail', TRUE);

   }

   $credential_query = $this->credential_query;

   // to get the credentials, we only need to supply  an ID, an email
   // or a username, but not all three. ID in this instance.
   $credentials = $this->resource_query($credential_query, array(
      ":id" => $id,
      ":email" => NULL,
      ":username" => NULL,
   )); 

   // if no user found by the id, fail
   if ($credentials === FALSE) {

     json_response('fail', array(
       'message' => 'Something went wrong',
     ));

   }

   // get the hash from the credentials
   $hash = get_val($credentials[0], 'hash', NULL);

   // check the password supplied
   if (!check_password($password, $hash)) {

     // fail if no match.
     json_response('fail', array(
       'message' => 'The password entered is incorrect.', 
     ));

   }

   // get the update query
   $query = $this->update_query;

   // supply new information to be updated.
   $result = $this->resource_query($query, array(
     ":id" => $id,
     ":username" => $username,
     ":email" => $email,
   ));

   // if no result (should be updated record id), fail
   if ($result === FALSE) {

     json_response('fail', array(
       'message' => 'Something went wrong.', 
     ));

   }

   // print the result in JSON
   json_response('data', $result);

 }

 // delete user form
 public function drop ($params = array()) {

   global $configs;

   $site_root = $configs['site_root'];

   $id = get_val($params, 1, 0);

   if (!$this->check_permission('destroy_user')) {

     go_home();

   }

   $query = $this->view_query;

   $result = $this->resource_query($query, array(
     ":id" => $id
   ));

   if ($result === FALSE) {

     include get_entity_partial('404');

     return $template;

   }

   $result = $result[0];

   $csrf_token = get_csrf_token_auth();

   $menu = $this->render_menu(
     array('id' => $id),
     array('view', 'edit', 'drop')
   );

   $username = get_val($result, 'username', '');

   $email = get_val($result, 'email', '');

   include get_entity_partial('drop');

   return $template;

 }

 public function destroy ($params = array()) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }
   
   global $user;

   $can = $this->check_permission('destroy_user');

   $user_id = get_val($user, 'id', 0);

   $id = get_val($params, 'user_id', 0);

   // all users can destroy their own profile
   if ($user_id != $id && !$can) {

     json_response('fail', TRUE);

   }

   $query = $this->destroy_query;

   $this->resource_query($query, array(
     ":user_id" => $id
   ));

   json_response('success', TRUE);

 }

 // authenticate an account via randomized url
 public function auth ($params = array()) {

   $auth = get_val($params, 2, NULL);

   if ($auth === NULL) {

     return;

   }

   $query = $this->auth_query;

   $result = $this->db->prepared_query($query, array(
     ":auth_url" => $auth,
   ));

   if (empty($result)) {

     go_home();

   }

   $result = $result[0];

   $error = get_val($result, 'error', NULL);

   if ($error !== NULL) {

     // provide error
     set_message($error, 'error');

     // go home
     go_home();

   }

   $id = $result['id'];
   
   // log in
   $_SESSION['id'] = $id;

   // go to profile
   redirect_entity_view('user', $id);

 }

 public function get_current_user () {

   $id = get_val($_SESSION, 'id', NULL);

   if ($id === NULL) {

     return NULL;

   }

   $query = $this->get_current_user_query;

   $result = $this->resource_query($query, array(
     ":id" => $id,
   ));

   if ($result === FALSE) {

     return NULL;

   }

   $user = $result[0];

   $permissions = $this->permission_controller->read_user_permissions(
     array('user_id' => $user['id'])
   );

   // reset the keys of permission array as the machine name (instead of
   // row number). allows for easier lookup later. O(1)
   foreach ($permissions as $id => $permission) {

     // use name, if by some anomaly it doesn't exist, use id
     $name = get_val($permission, 'name', $id);

     unset($permissions[$id]);

     $permissions[$name] = $permission;

   }

   $user['permissions'] = $permissions;

   return $user;

 }

 public function read_all ($params) {

   $can = $this->check_permission("read_every_user");

   if (!$can) {

     json_response("fail", TRUE);

   }

   $query = $this->read_all_query;

   $results = $this->resource_query($query);

   json_response("data", $results);

 }

 // returns rendered admin page
 public function administrate ($params) {

   global $user;

   $user_id = get_val($user, 'id', 0);

   $can = $this->check_permission("administrate_user");

   if (!$can) {

     go_home();

   }

   $js_template = $this->get_js($this, 'template');

   $js_controller = $this->get_js($this, 'administrate');

   include get_entity_partial("administrate");

   return $template;

 }

 public function change_status ($params) {

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }
 
   $can = $this->check_permission('block_user');

   if (!$can) {

     json_response('fail', TRUE);

   }

   $id = get_val($params, 'user_id', 0);

   $blocked = get_val($params, 'blocked', NULL);

   if ($blocked != NULL) {

     $blocked = $blocked == "false" ? 0: 1;

   }

   $query = $this->change_user_status_query; 

   $result = $this->resource_query($query, array(
     ":user_id" => $id,
     ":blocked" => $blocked,
   ));

   // todo modify query to return blocked user, so a different response logs
   // right now this is always true because the query returns no records
   if ($result === FALSE) {

     json_response("fail", TRUE);

   }

   json_response("success", TRUE);

 }

 public function get_action_title ($action, $params, $titles = array()) {

   $titles = array(
     'new' => 'Account Creation', 
     'edit' => 'Edit Account', 
     'view' => 'User Profile', 
     'drop' => 'Delete Account', 
     'administrate' => 'Administrate Users',
     'password_reset_form' => 'Password Reset',
   );

   return parent::get_action_title($action, $params, $titles);

 }

 public function get_action_css_class($action, $params, $classes = array()) {

   $classes = array(
     'new' => 'user-new',
     'view' => 'user-view',
     'edit' => 'user-edit',
     'drop' => 'user-drop',
     'administrate' => 'user-administrate',
     'password_reset_form' => 'user-password-reset-form',
     'password_reset' => 'user-password-reset',
   );

   return parent::get_action_css_class($action, $params, $classes);

 }

 public function is_current_user($params) {

   global $user;

   $id = get_val($params, 'id', 0);

   $current_user_id = get_val($user, 'id', 0);

   if ($id == $current_user_id) {

     return TRUE;

   }

   return FALSE;

 }

 protected function get_menu ($params) {

   global $configs;

   $site_root = get_val($configs, 'site_root', '/');

   $menu = parent::get_menu($params);

   global $user;

   $user_id = get_val($user, 'id', 0);

   $viewed_id = get_val($params, 'id', 0); 
   
   if ($viewed_id == $user_id) {
     $menu['view']['text'] = 'My Account';
   }

   $menu['edit']['text'] = 'Edit Info';

   unset($menu['edit']['permissions']);

   $menu['edit']['permission_callbacks'] = array(
     'is_current_user',
   );

   $menu['drop']['text'] = 'Delete Acount';

   $menu['password_reset_form'] = array(
     'text' => 'Reset Password',
     'url' => $site_root . '/user/password-reset',
     'permission_callbacks' => array('is_current_user')
   );

   return $menu;

 }

 public function password_reset ($params) {
   
   global $configs;

   $site_root = $configs['site_root'];

   global $user;

   if ($user == NULL) {

     json_response('fail', TRUE);

   }

   $csrf_token = get_val($params, 'csrf_token_auth', 0);

   if (!$this->check_csrf_token_auth($csrf_token)) {

     json_response('fail', TRUE);

   }

   $id = get_val($user, 'id', 0);

   $query = $this->password_reset_query;

   $password = get_val($params, 'password', NULL);

   $confirm = get_val($params, 'confirm', NULL);

   if ($password !== $confirm) {

     json_response('fail', array(
       'message' => 'Passwords must match',
     ));

   }

   $hash = encrypt_password($password);

   $result = $this->resource_query($query, array(
     ":id" => $id,
     ":hash" => $hash,
   ));

   json_response('data', $id);

 }

 public function password_reset_form ($params) {

   global $configs;

   $site_root = $configs['site_root'];

   global $user;

   if ($user == NULL) {

     go_home();

   }

   $csrf_token = get_csrf_token_auth();

   $id = get_val($user, 'id', 0);

   $menu = $this->render_menu(
     array('id' => $id),
     array('view')
   );

   $js_controller = $this->get_js($this, 'password_reset_form');

   include get_entity_partial('password_reset_form');

   return $template;

 }

}

?>
