app.template("admin_user_template", function (record) {

  record = record || {}

  var username, email, blocked;

  id = record.id;
  username = record.username;
  email = record.email;
  blocked = record.blocked ? 'checked="checked"': '';

  var elem = $('\
  <div data-user="'+ id +'" class="record user-'+id+'">\
    <span class="field username">'+username+'</span>\
    <span class="field email">'+email+'</span>\
    <span class="nowrap">\
      <label for="blocked">Blocked</label>\
      <input name="blocked" class="field blocked" type="checkbox" ' + blocked + '>\
    </span>\
    <div class="button destroy">Delete</div>\
  </div>\
  ');

  return elem;

});
