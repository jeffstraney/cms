app.controller( function(scope) {

  var api_endpoint = scope.api_endpoint;

  var update_user = function (params) {

    return api_endpoint(params, {
      url: '/user/update' 
    });

  };

  var form = $('form#user-edit');

  form.submit(function (e) {

    e.preventDefault();

    var self = $(this);

    var id = $('input.id', self).val(); 

    var username = $('input.username', self).val(); 

    var email = $('input.email', self).val(); 

    var password = $('input.password', self).val(); 

    var confirm = $('input.confirm', self).val(); 

    update_user({
      id: id,
      username: username,
      email: email,
      password: password,
    })
    .then(function (response) {

      response = response || {};

      var message, style, confirm_text, confirm_callback;

      if (response.fail) {

        var fail = response.fail
        fail = fail == true? {}: response.fail;

        message = fail.message || "Something went wrong";
        style = 'error';

      }
      else {
        message = "Your profile has been saved!";
        style = 'info';
        confirm_text = "See Profile";
        confirm_callback = function () {

          window.location = app.site_root + "/user/" + id;

        };
      }

      var modal = scope.modal_dialogue({
        message: message,
        cancel_text: "Close",
        cancel_callback: function () {
          
        },
        confirm_text: confirm_text,
        confirm_callback: confirm_callback,
        style: style
      });

      $('body').append(modal);

    });

  });

});
