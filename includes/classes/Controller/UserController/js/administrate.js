app.controller(function (scope) {

  var api_endpoint = scope.api_endpoint;

  var read_all_users = function (params) {

    return api_endpoint(params, {
      url: "/user/read_all"
    });

  };

  var destroy_user = function (params) {

    return api_endpoint(params, {
      url: "/user/destroy"
    });

  };

  var change_user_status = function (params) {

    return api_endpoint(params, {
      url: "/user/status"
    });

  };

  var user_aggregate = $("div.administrate-users.aggregate.user");

  var admin_user_template = scope.admin_user_template;

  function bind_user_events (elem) {

    var user_id = elem.attr('data-user');

    $('input.blocked', elem).change(function () {

      var status = this.checked;

      var self = $(this);

      change_user_status({user_id: user_id, blocked: status})
      .then(function (response) {

        response = response || {};

        if (response.fail) {

          return;

        }

        // do something

      });


    });

    $('.button.destroy', elem).click(function () {

      var confirm_destroy = function() {

        destroy_user({user_id: user_id})
        .then(function (response) {

          if (response.fail) {

            return;

          }

          elem.remove();

        });
      };

      var modal = scope.modal_dialogue({
        message: "Are you sure you want to delete user account?",
        style: "info",
        confirm_callback: confirm_destroy ,
        cancel_callback: function () {},
      });

      $(document.body).append(modal);
           
    });
  
  }

  // get all users
  read_all_users()
  .then(function (response) {

    response = response || {};

    var data = response.data || [];

    scope.apply_records({
      records: data,
      aggregate: user_aggregate,
      factory: admin_user_template,
      bind_events_callback: bind_user_events 
    });

  });

});
