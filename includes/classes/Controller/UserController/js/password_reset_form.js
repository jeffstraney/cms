app.controller(function (scope) {

  var api_endpoint = scope.api_endpoint;

  var password_reset = function (params) {

    return api_endpoint(params, {
      url: "/user/password-reset"
    });

  };

  $(function () {

    var form = $('form#user-password-reset');

    form.submit(function (e) {

      e.preventDefault();

      var password = $('input.password').val();

      var confirm = $('input.confirm').val();

      password_reset({
        password: password,
        confirm: confirm,
      })
      .then(function (response) {

        var message, cancel_text, style;

        if (response.fail) {


          message = response.fail.message;

          cancel_text = "Sorry";

          style = "error";

        }
        else {

          message = "Your password has been changed.";

          cancel_text = "Okay";

          style = "info";

        }

        var modal = scope.modal_dialogue({
          message: message,
          cancel_callback: function () {},
          cancel_text: cancel_text,
          style: style
        });

        $('body').append(modal);

      });

    });

  });

});
