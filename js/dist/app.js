(function () {

  var site_root = app.site_root;

  var scope = app.scope;

  scope.api_endpoint = function (params, configs) {

    // post parameters for body of request
    params = params || {};

    // configs for how to make post request(url, headers, options)
    configs = configs || {};

    url = configs.url || "";

    url = site_root + url;

    // always include the anonymous token
    var csrf_token_anon = app.scope.csrf_token_anon;
    params.csrf_token_anon = csrf_token_anon;

    // always include the authorized token
    var csrf_token_auth = app.scope.csrf_token_auth;
    params.csrf_token_auth = csrf_token_auth;

    // return the ajax call so it may be deferred/handled as a promise 
    return $.ajax({
      url: url,
      method: "POST",
      data: params
    });

  };

  // synonym for jquery document.ready
  scope.ready = function (callback) {

    $(callback);

  };

  // iterates through an array of objects.
  // constructs containers for each record
  // appends to a chosen container
  scope.apply_records = function (configs) {

    configs = configs || {};

    // container for records
    var aggregate = configs.aggregate || null;

    // factory function which returns a templated object
    var factory = configs.factory || function () {};

    // records are in an array
    var records = configs.records || [];

    var bind_events_callback = configs.bind_events_callback || function () {};

    if (aggregate == null) {

      console.error("apply_records expects jquery object for aggregate and function for factory");
      return;

    }

    for(var i = 0; i < records.length; i++) {

      var record = records[i];

      var elem = factory(record);

      bind_events_callback(elem);

      aggregate.append(elem);

    } 

  };

  // kind of like angular, but a bit simpler because it doesn't entail
  // watch/digest cycles. updating/making requests is entirely up to the
  // programmer
  app.controller = function (callback) {

    callback(scope);

  };

  // accepts a factory function as a callback, as well as the name
  // of the template 
  app.template = function (name, callback) {

    scope[name] = callback;

  };

})();


(function (a) {

  // widget functions will take an element, configurations and will
  // attach events to the element by reference. It is highly encouraged
  // (though not always necessary) to return the element to allow chaining


  // a text field widget which accepts another textfield as a source. when
  // that source is changed, this one automatically updates to have the same
  // text, but lowercase and underscore delimited.
  a.machine_name_text_field = function (elem, configs) {

    configs = configs || {};

    // source of the human readable name which will be translated to
    // a machine readable name. 
    var source = configs.source || null; 

    if (!source) {

      return elem;

    }

    // handler for keyup, change, and blur
    function process_text(e) {

      var self = $(this);

      var val = self.val();

      // converts any non alphanumeric character to underscore
      val = val.toLowerCase();

      val = val.replace(/\W+/g, "_");

      elem.val(val);

    }

    source.bind("keyup change input", process_text);

    return elem;

  };

})(app);

// example modal dialog template
app.template('modal_dialogue', function (configs) {

  configs = configs || {};

  message = configs.message || "";

  style = configs.style || "info";

  cancel_callback = configs.cancel_callback || null; 
  cancel_text = configs.cancel_text || "Cancel"; 
  confirm_callback = configs.confirm_callback || null; 
  confirm_text = configs.confirm_text || "Confirm"; 

  elem = $('\
  <div style="display:none" class="modal-dialogue '+ style +'">\
  <p>' + message + '</p>\
  </div>\
  ');

  if (cancel_callback) {

    var cancel = $('<div class="button cancel">'+ cancel_text +'</div>');

    cancel.click(function () {

      cancel_callback();

      elem.remove();
      
    });

    elem.append(cancel)

  }
  if (confirm_callback) {

    var confirm = $('<div class="button confirm">' + confirm_text + '</div>');

    confirm.click(function () {

      confirm_callback();

      elem.remove();

    });

    elem.append(confirm)

  }

  setTimeout(function () {

    elem.fadeIn(500);

    elem.addClass('loaded');

  }, 50);

  return elem;

});

app.template('code_editor', function (configs) {

  configs = configs || {};

  source = configs.source || "";

  //todo add code editor functionality

});

