(function () {

  var site_root = app.site_root;

  var scope = app.scope;

  scope.api_endpoint = function (params, configs) {

    // post parameters for body of request
    params = params || {};

    // configs for how to make post request(url, headers, options)
    configs = configs || {};

    url = configs.url || "";

    url = site_root + url;

    // always include the anonymous token
    var csrf_token_anon = app.scope.csrf_token_anon;
    params.csrf_token_anon = csrf_token_anon;

    // always include the authorized token
    var csrf_token_auth = app.scope.csrf_token_auth;
    params.csrf_token_auth = csrf_token_auth;

    // return the ajax call so it may be deferred/handled as a promise 
    return $.ajax({
      url: url,
      method: "POST",
      data: params
    });

  };

  // synonym for jquery document.ready
  scope.ready = function (callback) {

    $(callback);

  };

  // iterates through an array of objects.
  // constructs containers for each record
  // appends to a chosen container
  scope.apply_records = function (configs) {

    configs = configs || {};

    // container for records
    var aggregate = configs.aggregate || null;

    // factory function which returns a templated object
    var factory = configs.factory || function () {};

    // records are in an array
    var records = configs.records || [];

    var bind_events_callback = configs.bind_events_callback || function () {};

    if (aggregate == null) {

      console.error("apply_records expects jquery object for aggregate and function for factory");
      return;

    }

    for(var i = 0; i < records.length; i++) {

      var record = records[i];

      var elem = factory(record);

      bind_events_callback(elem);

      aggregate.append(elem);

    } 

  };

  // kind of like angular, but a bit simpler because it doesn't entail
  // watch/digest cycles. updating/making requests is entirely up to the
  // programmer
  app.controller = function (callback) {

    callback(scope);

  };

  // accepts a factory function as a callback, as well as the name
  // of the template 
  app.template = function (name, callback) {

    scope[name] = callback;

  };

})();

