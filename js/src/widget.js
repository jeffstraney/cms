(function (a) {

  // widget functions will take an element, configurations and will
  // attach events to the element by reference. It is highly encouraged
  // (though not always necessary) to return the element to allow chaining


  // a text field widget which accepts another textfield as a source. when
  // that source is changed, this one automatically updates to have the same
  // text, but lowercase and underscore delimited.
  a.machine_name_text_field = function (elem, configs) {

    configs = configs || {};

    // source of the human readable name which will be translated to
    // a machine readable name. 
    var source = configs.source || null; 

    if (!source) {

      return elem;

    }

    // handler for keyup, change, and blur
    function process_text(e) {

      var self = $(this);

      var val = self.val();

      // converts any non alphanumeric character to underscore
      val = val.toLowerCase();

      val = val.replace(/\W+/g, "_");

      elem.val(val);

    }

    source.bind("keyup change input", process_text);

    return elem;

  };

})(app);

// example modal dialog template
app.template('modal_dialogue', function (configs) {

  configs = configs || {};

  message = configs.message || "";

  style = configs.style || "info";

  cancel_callback = configs.cancel_callback || null; 
  cancel_text = configs.cancel_text || "Cancel"; 
  confirm_callback = configs.confirm_callback || null; 
  confirm_text = configs.confirm_text || "Confirm"; 

  elem = $('\
  <div style="display:none" class="modal-dialogue '+ style +'">\
  <p>' + message + '</p>\
  </div>\
  ');

  if (cancel_callback) {

    var cancel = $('<div class="button cancel">'+ cancel_text +'</div>');

    cancel.click(function () {

      cancel_callback();

      elem.remove();
      
    });

    elem.append(cancel)

  }
  if (confirm_callback) {

    var confirm = $('<div class="button confirm">' + confirm_text + '</div>');

    confirm.click(function () {

      confirm_callback();

      elem.remove();

    });

    elem.append(confirm)

  }

  setTimeout(function () {

    elem.fadeIn(500);

    elem.addClass('loaded');

  }, 50);

  return elem;

});

app.template('code_editor', function (configs) {

  configs = configs || {};

  source = configs.source || "";

  //todo add code editor functionality

});

