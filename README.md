<pre>
   ___  ___   _____   _____ ___  ___ _____ 
  |_  |/ _ \ /  ___| /  __ \|  \/  |/  ___|
    | / /_\ \\ `--.  | /  \/| .  . |\ `--. 
    | |  _  | `--. \ | |    | |\/| | `--. \
/\__/ / | | |/\__/ / | \__/\| |  | |/\__/ /
\____/\_| |_/\____/   \____/\_|  |_/\____/ 
</pre>

### A hands on PHP CMS for developers

* Written using an MVC design pattern
* Each controller strives to be atomic
* Uses REST to decouple controller logic


