<?php

// we will use this to store configurations to the web app
global $configs;

$configs = array();

// first level of keys in config is the various environments
// 'development', 'staging', 'production'
$configs['dev'] = array(
  // used in request url's
  'dir_root' => getcwd(),

  // used in request url's
  'site_root' => "http://localhost/cms",

  // used in request url's
  'site_title' => "Welcome",

  // directory relative to the includes/classes directory. Contains
  // partials for entity views and forms. Also includes queries for entities.
  'entity_partial_dir' => "partials",

  // database connection
  'connection' => array(
    'db' => 'cms',
    'host' => 'localhost',
    'port' => '3600',
    'user' => 'root',
    'password' => '',
    'driver' => 'mysql',
  ),

  // white listed html tags that can be used when a user has the
  // 'embed_restricted_html' permission. Do not ever add <script>
  // to this list, as it is a serious vulnerability
  'xss_white_list' => array(
    '<b>',
    '<a>',
  ),
);

// lastly, pick the environment like so, change per environment
$configs = $configs['dev'];

spl_autoload_register(function ($class) {

  $prefix = "Jasper\\";

  $len = strlen($prefix);

  $base_dir = __DIR__ . '/includes/classes/';

  if (strncmp($prefix, $class, $len) !== 0) {

    return;

  }

  $relative_class = substr($class, $len);

  $class_name = $arr = array_slice($array = explode('\\', $relative_class), -1, 1, TRUE);
  $class_name = array_pop($class_name);

  $file = $base_dir . str_replace('\\', '/', $relative_class) . '/' . $class_name . '.inc';

  if (file_exists($file)) {

    require $file;

  }

});



?>
