USE cms;

ALTER TABLE permission AUTO_INCREMENT = 1;

CALL create_role("administrator", "Administrator");
CALL create_role("authenticated_user", "Authenticated User");

