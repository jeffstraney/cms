88                                          88 88  
""                         ,d               88 88  
                           88               88 88  
88 8b,dPPYba,  ,adPPYba, MM88MMM ,adPPYYba, 88 88  
88 88P'   `"8a I8[    ""   88    ""     `Y8 88 88  
88 88       88  `"Y8ba,    88    ,adPPPPP88 88 88  
88 88       88 aa    ]8I   88,   88,    ,88 88 88  
88 88       88 `"YbbdP"'   "Y888 `"8bbdP"Y8 88 88  
                                                   
--------------------------------------------------------------------------------
To run an install script for the database, just run this command

./install

You may optionally provide flags to install the DB schema without any records 

./install --bare

alternatively, you can redirect the sql files into the mysql command

'mysql -u root < database.sql'

this will only work if the mysql executable is in a directory within your $PATH
environment variable. To determine this, run

echo $PATH

you will see a list of directories separated by ':'. check if any of these has
mysql in them. If not, research how to add it to your path.
--------------------------------------------------------------------------------
