USE cms;

ALTER TABLE permission AUTO_INCREMENT = 1;

-- permission
CALL create_permission("create_permission", "Create Permission");
CALL create_permission("read_permission", "Read Permission");
CALL create_permission("read_every_permission", "Read Every Permission");
CALL create_permission("update_permission", "Update Permission");
CALL create_permission("destroy_permission", "Destroy Permission");
CALL create_permission("administrate_permission", "Administrate Permission");

-- role 
CALL create_permission("create_permission", "Create Role");
CALL create_permission("read_role", "Read Role");
CALL create_permission("update_role", "Update Role");
CALL create_permission("destroy_role", "Destroy Role");
CALL create_permission("administrate_role", "Administrate Role");

-- user
CALL create_permission("create_permission", "Create User");
CALL create_permission("read_user", "Read User");
CALL create_permission("read_every_user", "Read Every User");
CALL create_permission("update_user", "Update User");
CALL create_permission("destroy_user", "Destroy User");
CALL create_permission("administrate_user", "Administrate User");
CALL create_permission("block_user", "Block User");

-- page
CALL create_permission("create_page", "Create Page");
CALL create_permission("read_page", "Read Page");
CALL create_permission("update_page", "Update Page");
CALL create_permission("destroy_page", "Destroy Page");
CALL create_permission("administrate_page", "Administrate Page");
CALL create_permission("read_restricted_page", "Read Restricted Page");
CALL create_permission("embed_full_html", "Embed Full HTML");
CALL create_permission("embed_restricted_html", "Embed Restricted HTML");

