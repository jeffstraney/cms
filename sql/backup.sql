-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 10, 2017 at 02:43 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `authenticate_user` (IN `_auth_url` VARCHAR(60))  BEGIN

  DECLARE _id INT(10);

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO `user` (username, email, hash)
    SELECT username, email, hash
      FROM temp_user
      WHERE auth_url = _auth_url;

    SET _id = LAST_INSERT_ID();

    DELETE FROM temp_user WHERE auth_url = _auth_url;

    SELECT _id AS id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_page` (IN `_title` VARCHAR(100), IN `_body` TEXT, IN `_restricted` INT(1), IN `_published` INT(1), IN `_path` VARCHAR(60))  BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO page (title, body, restricted, published, path)
    VALUES (_title, _body, _restricted, _published, _path);

    SELECT LAST_INSERT_ID() AS id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_permission` (IN `_permission_name` VARCHAR(60), IN `_permission_label` VARCHAR(60))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    INSERT INTO permission (name, label)
    VALUES (_permission_name, _permission_label);

    SELECT LAST_INSERT_ID() AS id, _permission_name AS name, _permission_label AS label;

  COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_role` (IN `_name` VARCHAR(60), IN `_label` VARCHAR(60))  BEGIN

  DECLARE _role_id INT;

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO role (name, label)
    VALUES (_name, _label);

    SET _role_id = LAST_INSERT_ID();

    SELECT _role_id AS id, _name AS name, _label AS label;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_user` (IN `_username` VARCHAR(60), IN `_email` VARCHAR(60), IN `_hash` VARCHAR(60), IN `_auth_url` VARCHAR(60))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO temp_user (username, email, hash, auth_url)
    VALUES (_username, _email, _hash, _auth_url);

    SELECT 1 AS success;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `destroy_permission` (IN `_permission_id` INT)  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    DELETE FROM permission 
    WHERE _permission_id = id;

    SELECT _permission_id AS id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `destroy_role` (IN `_role_id` SMALLINT(5))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    DELETE FROM role
    WHERE id = _role_id;

    SELECT 1 AS success;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_hash` (IN `_credential` VARCHAR(60))  BEGIN
  SELECT u.id, u.hash
  FROM `user` u
  WHERE u.username = _credential OR u.email = _credential;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `grant_role_permission` (IN `_role_id` SMALLINT(5), IN `_permission_id` INT)  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    INSERT INTO role_permission (role_id, permission_id)
    VALUES (_role_id, _permission_id);

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `read_permission_roles` (IN `_permission_id` INT)  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    SELECT r.name, r.label, r.id, rp.role_id AS has_permission
    FROM role r
    LEFT JOIN role_permission rp ON r.id = rp.role_id
    AND rp.permission_id = _permission_id
    UNION
    SELECT r.name, r.label, r.id, NULL AS has_permission
    FROM role r
    WHERE NOT EXISTS (
      SELECT rp.role_id
      FROM role_permission rp
      WHERE r.id = rp.role_id
      AND rp.permission_id = _permission_id);

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `record_login` (IN `_user_id` INT(10))  BEGIN
  START TRANSACTION;

    UPDATE `user` SET
    last_login = NOW()
    WHERE id = _user_id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `revoke_role_permission` (IN `_role_id` SMALLINT(5), IN `_permission_id` SMALLINT(5))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    DELETE FROM role_permission
    WHERE role_id = _role_id
    AND permission_id = _permission_id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_page` (IN `_id` INT(10), IN `_title` VARCHAR(60), IN `_body` TEXT, IN `_restricted` INT(1), IN `_published` INT(1), IN `_path` VARCHAR(60))  BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE page SET 
    title = _title,
    body = _body,
    restricted = _restricted,
    published = _published,
    path = _path
    WHERE id = _id;

    SELECT _id AS id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_permission` (IN `_permission_id` INT, IN `_label` VARCHAR(100), IN `_name` VARCHAR(100))  BEGIN
 DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  START TRANSACTION;

    UPDATE permission SET
    label = _label,
    name = _name
    WHERE id = _permission_id;

    SELECT _permission_id AS id;

  COMMIT;
  
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_role` (IN `_role_id` SMALLINT(5), IN `_name` VARCHAR(60), IN `_label` VARCHAR(60))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE role SET
    name = _name,
    label = _label
    WHERE id = _role_id;

    SELECT id, name, label FROM role WHERE id = _role_id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_user` (IN `_id` INT(10), IN `_username` VARCHAR(60), IN `_email` VARCHAR(60))  BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    UPDATE user SET
    username = _username,
    email = _email
    WHERE id = _id;

    SELECT _id AS id;

  COMMIT;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_has_permission` (IN `_user_id` INT(10), IN `_permission_name` VARCHAR(60))  BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_text TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_text = MESSAGE_TEXT;
    SELECT _err_text AS err_text;
  END;

  SELECT u.id
  FROM `user` u, user_role ur, role_permission rp, permission p
  WHERE u.id = _user_id
  AND ur.user_id = u.id
  AND ur.role_id = rp.role_id
  AND rp.permission_id = p.id
  AND p.name = _permission_name;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `view_page` (IN `_id` INT(10), IN `_path` VARCHAR(60))  BEGIN
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    DECLARE _c_num INT;
    DECLARE _err_msg TEXT;
    ROLLBACK;
    GET DIAGNOSTICS _c_num = NUMBER;
    GET DIAGNOSTICS CONDITION _c_num _err_msg = MESSAGE_TEXT;
    SELECT _err_msg AS error;
  END;

  START TRANSACTION;

    SELECT id, title, body, restricted, published, path
    FROM page
    WHERE id = _id
    OR path = _path;

  COMMIT;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL COMMENT 'visibly rendered title on page',
  `body` text NOT NULL,
  `restricted` tinyint(1) UNSIGNED NOT NULL COMMENT 'only users with ''view_restricted_page'' permission can view restricted pages',
  `published` tinyint(1) UNSIGNED NOT NULL COMMENT 'users with ''view_page'' permission can see page only if it is published and they do not have ''view_restricted_page'' permission',
  `path` varchar(60) NOT NULL COMMENT 'human readable path for pretty printed urls'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `title`, `body`, `restricted`, `published`, `path`) VALUES
(1, 'Welcomen', 'Welcome to your homepage, to start editing it, click the ''Edit'' link above.\n', 0, 1, 'home'),
(2, 'About', 'Edit this page to contain info about the site.', 0, 1, 'about'),
(3, 'Help', 'This page is to document help on the website. contain how-tos and useful links.', 0, 1, 'help'),
(4, 'Terms & Conditions', 'Edit this to contain your site''s terms and conditions.', 0, 1, 'agreement'),
(5, 'Privacy', 'Edit this to contain your privacy statement (e.g. no cookies are stored on this site)', 0, 1, 'privacy');

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `name`, `label`) VALUES
(1, 'create_permission', 'Create Permission'),
(2, 'update_permission', 'Update Permission'),
(3, 'destroy_permission', 'Destroy Permission'),
(4, 'read_all_permissions', 'Read All Permissions'),
(5, 'view_user', 'View User'),
(6, 'create_user', 'Create User'),
(7, 'update_user', 'Update User'),
(8, 'destroy_user', 'Destroy User'),
(9, 'administrate_users', 'Administrate Users'),
(10, 'read_all_users', 'Read All Users'),
(11, 'block_user', 'Block User'),
(12, 'administrate_roles', 'Administrate Roles'),
(13, 'read_all_roles', 'Read All Roles'),
(14, 'read_role', 'Read Role'),
(15, 'create_role', 'Create Role'),
(16, 'update_role', 'Update Role'),
(17, 'destroy_role', 'Destroy Role'),
(18, 'create_page', 'Create Page'),
(19, 'update_page', 'Update Page'),
(20, 'destroy_page', 'Destroy Page'),
(21, 'view_page', 'View Page'),
(22, 'administrate_pages', 'Administrate Pages');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(125) NOT NULL,
  `label` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `label`) VALUES
(1, 'administrator', 'Administrator'),
(2, 'authenticated_user', 'Authenticated User');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` smallint(5) UNSIGNED NOT NULL,
  `permission_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(2, 21);

-- --------------------------------------------------------

--
-- Table structure for table `temp_user`
--

CREATE TABLE `temp_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `hash` varchar(60) NOT NULL,
  `auth_url` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `hash` varchar(60) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `date_joined` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `hash`, `blocked`, `date_joined`, `last_login`) VALUES
(7, 'jeff', 'jeffjeffjeff@gmail.com', '$2y$12$b0JJ7ct7WGYk3kLM.SX8A.W/Vv6N2.f28Zvdp4vzeG6NEEdNpV7.m', 0, '2017-07-25 13:52:24', '2017-08-10 08:09:19'),
(9, 'josh', 'josh@gmail', '$2y$12$jqzMOmREEgSV6hcbgGeVt.HdasKJS8aspuazifv9SiaJKmSM3HZJK', 0, '2017-07-25 13:52:24', '2017-07-26 11:46:16'),
(10, 'jill', 'jill@gmail.com', '$2y$12$0EItn8xlfjfGYAbiqG90KeR7CSu2t6ESTto1/CzY1pCfS66NVDtwy', 0, '2017-08-09 08:54:16', '2017-08-09 13:32:15');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(7, 1),
(10, 2),
(9, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `path` (`path`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `temp_user`
--
ALTER TABLE `temp_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `auth_url` (`auth_url`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD UNIQUE KEY `email_2` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `temp_user`
--
ALTER TABLE `temp_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_permission_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
