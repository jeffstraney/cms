USE cms;

-- administrator permissions
CALL grant_role_permission(1, 1);
CALL grant_role_permission(1, 2);
CALL grant_role_permission(1, 3);
CALL grant_role_permission(1, 4);
CALL grant_role_permission(1, 5);
CALL grant_role_permission(1, 6);
CALL grant_role_permission(1, 7);
CALL grant_role_permission(1, 8);
CALL grant_role_permission(1, 9);
CALL grant_role_permission(1, 10);
CALL grant_role_permission(1, 11);
CALL grant_role_permission(1, 12);
CALL grant_role_permission(1, 13);
CALL grant_role_permission(1, 14);
CALL grant_role_permission(1, 15);
CALL grant_role_permission(1, 16);
CALL grant_role_permission(1, 17);
CALL grant_role_permission(1, 18);
CALL grant_role_permission(1, 19);
CALL grant_role_permission(1, 20);
CALL grant_role_permission(1, 21);
CALL grant_role_permission(1, 22);
CALL grant_role_permission(1, 23);
CALL grant_role_permission(1, 24);
CALL grant_role_permission(1, 25);
CALL grant_role_permission(1, 26);

-- authenticated user permissions
CALL grant_role_permission(2, 13);
CALL grant_role_permission(2, 20);
